import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultaComponent } from './pages/escrituras/consulta/consulta.component';
import { ConsultaModule } from './pages/escrituras/consulta/consulta.module';
import { PagesComponent } from './pages/pages.component';

const routes: Routes = [
  { path: '', component: PagesComponent, loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule) },
  /* { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  { path: 'cadastro', loadChildren: () => import('./pages/cadastro/cadastro.module').then(m => m.CadastroModule) },
  { path: 'atualizar-senha', loadChildren: () => import('./pages/atualizarsenha/atualizarsenha.module').then(m => m.AtualizarsenhaModule) },
  { path: 'recuperar-senha', loadChildren: () => import('./pages/recuperarsenha/recuperarsenha.module').then(m => m.RecuperarsenhaModule) },
 */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
