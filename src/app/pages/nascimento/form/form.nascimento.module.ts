import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingNascimentoModule } from './form-routing.nascimento.module';
import { FormNascimentoComponent } from './form.nascimento.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';


@NgModule({
  declarations: [
    FormNascimentoComponent,
    ConfirmDialogComponent
  ],
  imports: [
    CommonModule,
    FormRoutingNascimentoModule,
    MatFormFieldModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    NgxMaskModule.forRoot(),
    FormsModule,
    MatDialogModule,
    MatSelectModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatCheckboxModule
  ]
})
export class FormNascimentoModule { }

