import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormNascimentoComponent } from './form.nascimento.component';

const routes: Routes = [
  {path: '', component: FormNascimentoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormRoutingNascimentoModule { }
