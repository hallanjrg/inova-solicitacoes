import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNascimentoComponent } from './form.nascimento.component';

describe('FormNascimentoComponent', () => {
  let component: FormNascimentoComponent;
  let fixture: ComponentFixture<FormNascimentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormNascimentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNascimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
