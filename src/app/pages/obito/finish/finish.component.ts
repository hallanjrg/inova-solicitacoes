import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InfoService } from 'src/app/services/info.service';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.scss']
})
export class FinishComponent implements OnInit {
  public codigoCartorio: any;
  codigo
  today
  constructor(private route: Router,
    private activatedRoute: ActivatedRoute,
    public infoService: InfoService) {
      this.codigoCartorio = this.activatedRoute.snapshot.queryParams.codigoCartorio
     }

  ngOnInit(): void {
    this.codigo = this.activatedRoute.snapshot.params.id
    this.today = new Date()
  }

  backConsulta(){
    this.route.navigate([`/certidoes/consulta/${this.codigoCartorio}`], {});
  }

  sendHome() {
    this.route.navigate(['/obito'], {
      queryParams: {
        codigoCartorio: this.codigoCartorio,
      },
    });
  }

}
