import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormObitoComponent } from './form.obito.component';

describe('FormObitoComponent', () => {
  let component: FormObitoComponent;
  let fixture: ComponentFixture<FormObitoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormObitoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormObitoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
