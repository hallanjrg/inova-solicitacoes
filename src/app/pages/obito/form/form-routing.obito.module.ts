import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormObitoComponent } from './form.obito.component';

const routes: Routes = [
  {path: '', component: FormObitoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormRoutingObitoModule { }
