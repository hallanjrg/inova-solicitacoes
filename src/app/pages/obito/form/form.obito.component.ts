import { ActivatedRoute } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { cnpj, cpf } from 'cpf-cnpj-validator';
import { buscaCep } from 'src/app/models/busca-cep';
import { NewSolicitacao } from 'src/app/models/solicitacao';
import { ApiService } from 'src/app/services/api.service';
import { BuscaCepService } from 'src/app/services/busca-cep.service';
import { CepService } from 'src/app/services/cep.service';
import { InfoService } from 'src/app/services/info.service';
import { LocateService } from 'src/app/services/locate.service';
import validator from 'validar-telefone';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { Token } from 'src/app/models/token-interface';
import { Cartorio } from 'src/app/models/cartorio';
import { Location } from '@angular/common';
import { ConfirmModalCertificateComponent } from '../../escrituras/solicitacao-form/confirm-modal-certificate/confirm-modal-certificate.component';
import * as moment from 'moment';
import { timeout } from 'rxjs/operators';


@Component({
  selector: 'app-form',
  templateUrl: './form.obito.component.html',
  styleUrls: ['./form.obito.component.scss']
})
export class FormObitoComponent implements OnInit {
  public valorCertidao: any;
  public valorAdicional: any;
  public nomeCartorio: any;
  public cidadeCartorio: any;
  public listaResponsavel = [];
  public ufCartorio: any;
  public checkNomeCartorio = false;
  public checkEnderecoEntrega = false;
  public toGo: NewSolicitacao
  public error = {
    cpfCnpj: {
      message: ''
    },
    phone: {
      message: ''
    },
    email: {
      message: ''
    }
  }
  profileForm = this.fb.group({
    certidaoDigital: [false],
    codigoCartorio: '',
    nomeCartorio: [''],
    cidadeCartorio: [''],
    qtdAverbacao: [0],
    ufCartorio: [''],
    identificacaoCertidao: this.fb.group({
      cpf: [''],
      nomeCompleto: [''],
      nomeConjuge1: [''],
      nomeConjuge2: [''],
      dataNascimentoCasamento: [''],
    }),
    dados1: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ), dados2: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ), dados3: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ), dados4: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ), dados5: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ),
    tipoRetirada: [0],
    endereco: this.fb.group({
      cep: [''],
      logradouro: [''],
      numero: [''],
      complemento: [''],
      bairro: [''],
      cidade: [''],
      uf: [''],
      valorFrete: [0],
      retirar: [0]
    }),
    enderecoSolicitante: this.fb.group({
      cep: [''],
      logradouro: [''],
      numero: [''],
      complemento: [''],
      bairro: [''],
      cidade: [''],
      uf: [''],
    }),
    solicitante: this.fb.group({
      cpfCnpj: [''],
      responsavel: [''],
      nome: [''],
      telefone: [''],
      email: ['', ([Validators.required, Validators.email])],
    }),
    mensagem: [''],

  })
  public siglaSelecionada: string = ''
  public checked: boolean = false

  constructor(private fb: FormBuilder,
    public infoService: InfoService,
    public locateService: LocateService,
    private apiService: ApiService,
    public buscaCepService: BuscaCepService,
    private cdref: ChangeDetectorRef,
    private dialog: MatDialog,
    private cepService: CepService,
    private route: ActivatedRoute,
    public location: Location) {
    this.toGo = new NewSolicitacao()
  }

  ngOnInit(): void {
    let c = this.route.snapshot.queryParams['codigoCartorio']
    this.apiService.getToken(`token`, c).subscribe((res: Token) => {
      localStorage.setItem('token', res.token);
      this.apiService.getCartorioAtual(`cartorio/get`, c).subscribe((res: Cartorio) => {
        this.nomeCartorio = res.nome;
        this.cidadeCartorio = res.cidade;
        this.ufCartorio = res.estado;
        this.apiService.setCartorio(res)
        this.addValorCartorio(res.codigo);
      })
      this.locateService.getEstados()
      this.listarResponsavel();
    })

  }

  addValorCartorio(codigoCartorio){
    this.apiService.getApi(`cartorio/obter-configuracoes?codigoCartorio=${codigoCartorio}`).subscribe((res: any) => {
      this.valorAdicional = res.valorAdicional;
      let obter = res.valoresSolicitacoes
       obter.forEach(element => {
        if(element.tipoSolicitacao == 'CERTIDAO_OBITO'){
          let i = 0
          this.valorCertidao = element.tipoValor[i].valor
        }
      });
    })
  }

  listarResponsavel() {
    this.apiService.getApi('usuario/listar-recebedosite').subscribe((r: Array<any>) => {
      this.listaResponsavel = r;
    })
  }

  validateCertificate() {
    if (this.profileForm.value.certidaoDigital == true) {
      this.openConfirmModalCertificate()
    } else {
      this.submit(true)
    }
  }

  removeField() {
    if (this.buscaCepService.qtdSolicitacoes > 0) {
      this.buscaCepService.qtdSolicitacoes -= 1
      this.buscaCepService.valorServico = this.valorCertidao * this.buscaCepService.qtdSolicitacoes
    }
  }

  openConfirmModalCertificate() {
    let dialogRef = this.dialog.open(ConfirmModalCertificateComponent, {
      width: '600px',
      data: this.profileForm.value
    });
    dialogRef.afterClosed().subscribe(res => {
      this.submit(res)
    });
  }

  checkedEndereco(enderecoSolicitante){
    if(this.checkEnderecoEntrega == false){
      this.profileForm.get('enderecoSolicitante').get('logradouro').setValue(enderecoSolicitante.value.logradouro)
    this.profileForm.get('enderecoSolicitante').get('numero').setValue(enderecoSolicitante.value.numero)
    this.profileForm.get('enderecoSolicitante').get('cep').setValue(enderecoSolicitante.value.cep)
    this.profileForm.get('enderecoSolicitante').get('complemento').setValue(enderecoSolicitante.value.complemento)
    this.profileForm.get('enderecoSolicitante').get('bairro').setValue(enderecoSolicitante.value.bairro)
    this.profileForm.get('enderecoSolicitante').get('cidade').setValue(enderecoSolicitante.value.cidade)
    this.profileForm.get('enderecoSolicitante').get('uf').setValue(enderecoSolicitante.value.uf)
    this.checkEnderecoEntrega = true
    }else{
    this.profileForm.get('enderecoSolicitante').get('logradouro').setValue(null)
    this.profileForm.get('enderecoSolicitante').get('numero').setValue(null)
    this.profileForm.get('enderecoSolicitante').get('cep').setValue(null)
    this.profileForm.get('enderecoSolicitante').get('complemento').setValue(null)
    this.profileForm.get('enderecoSolicitante').get('bairro').setValue(null)
    this.profileForm.get('enderecoSolicitante').get('cidade').setValue(null)
    this.profileForm.get('enderecoSolicitante').get('uf').setValue(null)
    this.checkEnderecoEntrega = false
    }

  }

  addFields() {
    if (this.buscaCepService.qtdSolicitacoes < 6) {
      this.buscaCepService.qtdSolicitacoes += 1
      this.buscaCepService.valorServico = this.valorCertidao * this.buscaCepService.qtdSolicitacoes
    }
  }

  removeSelectedFile(index){
    let arquivos = [];
    arquivos = this.infoService.files;
    let i = arquivos.indexOf( index );
    arquivos.splice(i, 1);
  }


  change(nome,estado,cidade){
    if(this.checkNomeCartorio == false && this.profileForm.controls.nomeCartorio.value == ''){
      this.profileForm.controls.nomeCartorio.setValue(nome)
      this.profileForm.controls.cidadeCartorio.setValue(cidade)
      this.profileForm.controls.ufCartorio.setValue(estado)
      this.locateService.getMunicipiosPorEstado(this.profileForm.get('ufCartorio').value)
      this.checkNomeCartorio = true;
    }else{
      this.profileForm.controls.nomeCartorio.setValue('')
      this.profileForm.controls.cidadeCartorio.setValue('')
      this.profileForm.controls.ufCartorio.setValue('')
      this.checkNomeCartorio = false;
    }
  }

  phoneNumberIsReal(data) {
    if (data.length >= 10) {
      if (!validator(data)) {
        this.error.phone.message = 'O telefone digitado não é válido.'
        this.profileForm.get('solicitante').get('telefone').setErrors({ 'invalid': true })
      } else {
        this.error.phone.message = ''
      }
    } else {
      this.error.phone.message = 'Este campo é obrigatório.'
    }
  }

  cpfCnpjIsReal(data) {
    if (data.length > 11) {
      if (!cnpj.isValid(data)) {
        this.error.cpfCnpj.message = 'O CNPJ digitado não é válido.'
      } else {
        this.error.cpfCnpj.message = ''
      }
    } else if (data.length == 11) {
      if (!cpf.isValid(data)) {
        this.error.cpfCnpj.message = 'O CPF digitado não é válido.'
      } else {
        this.error.cpfCnpj.message = ''
      }
    }
    else {
      this.error.cpfCnpj.message = 'Este campo é obrigatório.'
    }
  }

  validacaoEmail(field: string) {
    if (field) {
      let usuario = field.substring(0, field.indexOf("@"));
      let dominio = field.substring(field.indexOf("@") + 1, field.length);
      if ((usuario.length >= 1) &&
        (dominio.length >= 3) &&
        (usuario.search("@") == -1) &&
        (dominio.search("@") == -1) &&
        (usuario.search(" ") == -1) &&
        (dominio.search(" ") == -1) &&
        (dominio.search(".") != -1) &&
        (dominio.indexOf(".") >= 1) &&
        (dominio.lastIndexOf(".") < dominio.length - 1)) {
        this.error.email.message = ''
        this.profileForm.get('solicitante').get('email').setErrors(null)
      }
      else {
        this.error.email.message = 'O e-mail digitado não é válido.'
        this.profileForm.get('solicitante').get('email').setErrors({ 'invalid': true })
      }
    }
  }

  get phoneMask() {
    return this.isPhone() ? '(00) 0000-00009' : '(00) 00000-0000'
  }

  isPhone() {
    return this.profileForm.get('solicitante').get('telefone').value.length <= 10
  }

  public cpfcnpjmask = function (rawValue) {
    let numbers = rawValue.match(/\d/g);
    let numberLength = 0;
    if (numbers) {
      numberLength = numbers.join('').length;
    }
    if (numberLength <= 11) {
      return [/[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    } else {
      return [/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    }
  }



  submit(cancelButtonClicked?) {
    this.phoneNumberIsReal(this.profileForm.value['solicitante'].telefone)
    this.validacaoEmail(this.profileForm.value['solicitante'].email)
    this.cpfCnpjIsReal(this.profileForm.value['solicitante'].cpfCnpj)
    this.infoService.summaryCheck = this.profileForm.value
    this.buscaCepService.summary = this.profileForm.value
    this.toGo.dados = []
    for (let index = 0; index < this.buscaCepService.qtdSolicitacoes; index++) {
      const element = this.toGo.dados[index];
      let dado = {
        ...this.profileForm.get(`dados${index + 1}`).value
      }
      if (dado.dataAto) {
        let y = dado.dataAto.substr(0, 4)
        let m = dado.dataAto.substr(5, 2)
        let d = dado.dataAto.substr(8, 2)
        dado.dataAto = moment(new Date(y, m - 1, d)).format('YYYY-MM-DD')
      }
      this.toGo.dados.push(dado)
    }
    this.toGo.certidaoDigital = this.profileForm.value.certidaoDigital
    this.toGo.dadosSolicitante = this.profileForm.get('solicitante').value
    this.toGo.identificacaoCertidao = this.profileForm.get('identificacaoCertidao').value
    this.toGo.cidadeCertidao = this.profileForm.get('cidadeCartorio').value
    this.toGo.cartorioCertidao = this.profileForm.get('nomeCartorio').value
    this.toGo.endereco = this.profileForm.get('endereco').value
    this.toGo.enderecoSolicitante = this.profileForm.get('enderecoSolicitante').value
    this.toGo.mensagem = this.profileForm.get('mensagem').value
    this.toGo.tipoSolicitacao = 'CERTIDAO_OBITO'
    this.toGo.tipoRetirada = this.profileForm.get('endereco').get('retirar').value
    this.toGo.dadosSolicitante.nome = this.toGo.dadosSolicitante.nome.replaceAll("'", '').replaceAll('.', '').replaceAll('-', '')
    this.toGo.valorFrete = this.buscaCepService.frete
    this.toGo.qtdAverbacao = this.profileForm.value.qtdAverbacao
    this.toGo.valorSolicitacao = this.valorCertidao * this.toGo.dados.length
    if(this.toGo.certidaoDigital){
      this.toGo.valorSolicitacao = this.toGo.valorSolicitacao + this.valorAdicional
    }
    this.toGo.arquivos = this.infoService.files
    let a = []
    this.toGo.arquivos.forEach(element => {
      a.push(element.codigoArquivo)
    });
    this.toGo.arquivos = a

    this.openConfirm()
  }

  openConfirm() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '600px',
      data: this.toGo
    })
  }



  getAddressOnly(params) {
    if (params.length === 8) {
      this.cepService.setAddress(params).subscribe((res: buscaCep) => {
        this.profileForm.get('enderecoSolicitante').get('logradouro').setValue(res.logradouro)
        this.profileForm.get('enderecoSolicitante').get('bairro').setValue(res.bairro)
        this.profileForm.get('enderecoSolicitante').get('uf').setValue(res.uf)
        if (this.profileForm.controls.enderecoSolicitante.value.uf != "") {
          this.locateService.getMunicipiosPorEstado(this.profileForm.controls.enderecoSolicitante.value.uf)
        }
        this.profileForm.get('enderecoSolicitante').get('cidade').setValue(res.localidade)
      })
    }
  }

  getCep(params, tipoEndereco) {
    if (params.length == 8) {
      this.apiService.getApi(`correios/calcularfrete?origem=${this.apiService.chosenCartorio.cep}&destino=${params}`)
        .pipe(timeout(10000)).subscribe((res1: any) => {
          this.setNewAddress(params, res1);
        }, err => {
          this.setNewAddress(params);
        })
    }
  }

  setNewAddress(params, res1?) {
    this.cepService.setAddress(params).subscribe((res: buscaCep) => {
      this.profileForm.get('endereco').get('logradouro').setValue(res.logradouro)
      this.profileForm.get('endereco').get('bairro').setValue(res.bairro)
      this.profileForm.get('endereco').get('uf').setValue(res.uf)
      if (this.profileForm.controls.endereco.value.uf != "") {
        this.locateService.getMunicipiosPorEstado(this.profileForm.controls.endereco.value.uf)
      }
      this.profileForm.get('endereco').get('cidade').setValue(res.localidade)
      this.buscaCepService.frete = res1?.valor || 0
      this.buscaCepService.calculaTotal(res1?.valor || 0)
    })
  }

  onClick(info) {
    if (info === 'Entregar no endereço') {
      this.profileForm.get('endereco').get('logradouro').setValidators([Validators.required])
      this.profileForm.get('endereco').get('numero').setValidators([Validators.required])
      this.profileForm.get('endereco').get('bairro').setValidators([Validators.required])
      this.profileForm.get('endereco').get('cidade').setValidators([Validators.required])
      this.profileForm.get('endereco').get('uf').setValidators([Validators.required])
      this.profileForm.get('endereco').get('retirar').setValue(1)
      this.infoService.forma_entrega = 1
    } else {
      this.profileForm.get('endereco').get('cep').clearValidators();
      this.profileForm.get('endereco').get('logradouro').clearValidators();
      this.profileForm.get('endereco').get('numero').clearValidators();
      this.profileForm.get('endereco').get('bairro').clearValidators();
      this.profileForm.get('endereco').get('cidade').clearValidators();
      this.profileForm.get('endereco').get('uf').clearValidators();
      this.profileForm.get('endereco').get('cep').setValue('')
      this.infoService.opcaoEntregaSelecionada = 'Retirar no cartório'
      this.buscaCepService.calculaTotal(0)
      this.buscaCepService.frete = 0
      this.profileForm.get('endereco').get('cep').updateValueAndValidity();
      this.profileForm.get('endereco').get('logradouro').updateValueAndValidity();
      this.profileForm.get('endereco').get('numero').updateValueAndValidity();
      this.profileForm.get('endereco').get('bairro').updateValueAndValidity();
      this.profileForm.get('endereco').get('cidade').updateValueAndValidity();
      this.profileForm.get('endereco').get('uf').updateValueAndValidity();
      this.profileForm.get('endereco').get('retirar').setValue(0)
      this.infoService.forma_entrega = 0
    }
  }


}
