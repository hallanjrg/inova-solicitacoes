import { ToastService } from 'src/app/services/toast/toast-service';
import { ModalcadastroComponent } from './modalcadastro/modalcadastro.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { cpf, cnpj } from 'cpf-cnpj-validator';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {
  public cadastro: FormGroup;
  public toast: any
  public error = {
    cpfCnpj: {
      message: ''
    },
    phone: {
      message: ''
    },
    email: {
      message: ''
    }
  }


  constructor(
    private toastService: ToastService,
    public fb: FormBuilder,
    private router: Router,
    private modal: NgbModal,
    private api: ApiService) {
    this.cadastro = this.fb.group({
      nome: [null, Validators.required],
      email: [null, Validators.required],
      usuarioLogin: [null, Validators.required],
    })
  }

  ngOnInit(): void {
  }

  validar() {
    if (this.cadastro.invalid) {
      this.toast = this.toastService.show('Preencha os campos corretamente')
    } else {
      /* const modalRed = this.modal.open(ModalcadastroComponent) */
    }
  }

  back() {
    this.router.navigate(['login'])
  }

  cpfCnpjIsReal(data: any) {
    if (data.length > 11) {
      if (!cnpj.isValid(data)) {
        this.error.cpfCnpj.message = 'O CNPJ digitado não é válido.'
      } else {
        this.error.cpfCnpj.message = ''
      }
    } else if (data.length == 11) {
      if (!cpf.isValid(data)) {
        this.error.cpfCnpj.message = 'O CPF digitado não é válido.'
        this.cadastro.controls.cpfCnpj.setErrors({ 'invalid': true })
        this.cadastro.controls.cpfCnpj.markAsTouched()
      } else { }
    }
  }

  validacaoEmail(data: any) {
    if (data) {
      let usuario = data.substring(0, data.indexOf('@'));
      let dominio = data.substring(data.indexOf('@') + 1, data.length);
      if (
        usuario.length >= 1 &&
        dominio.length >= 3 &&
        usuario.search('@') == -1 &&
        dominio.search('@') == -1 &&
        usuario.search(' ') == -1 &&
        dominio.search(' ') == -1 &&
        dominio.search('.') != -1 &&
        dominio.indexOf('.') >= 1 &&
        dominio.lastIndexOf('.') < dominio.length - 1
      ) { }
    }
  }

}
