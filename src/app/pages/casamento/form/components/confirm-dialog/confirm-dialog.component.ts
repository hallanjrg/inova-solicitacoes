import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { BuscaCepService } from 'src/app/services/busca-cep.service';
import { InfoService } from 'src/app/services/info.service';
import { LoadingService } from 'src/app/services/loading.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
  public codigoCartorio: any;

  constructor(public infoService: InfoService,
    public buscaCepService: BuscaCepService,
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    public loadingService: LoadingService,
    public tokenService: TokenService,
    private route: ActivatedRoute,
    private apiService: ApiService,
    @Inject(MAT_DIALOG_DATA) public data,
    private router: Router) {
      this.codigoCartorio = this.route.snapshot.queryParams.codigoCartorio;
    }

  ngOnInit(): void {
  }

  public cpfcnpjmask = function (rawValue) {
    let numbers = rawValue.match(/\d/g);
    let numberLength = 0;
    if (numbers) {
      numberLength = numbers.join('').length;
    }
    if (numberLength <= 11) {
      return [/[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    } else {
      return [/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    }
  }
  submit() {
    this.infoService.clicked = true
    this.apiService.postApi(`solicitacaocertidao/save`, this.data).subscribe(res => {
      this.dialogRef.close()
      this.router.navigate(['casamento/finish/' + res],{
        queryParams: {
          codigoCartorio: this.codigoCartorio,
        },
      })
    }, err => {
    })
  }

  showFreightValue() {

    let freight = this.buscaCepService?.frete == 0 ? 0 : this.buscaCepService?.frete
    return freight
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
