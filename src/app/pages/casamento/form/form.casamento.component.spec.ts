import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCasamentoComponent } from './form.casamento.component';

describe('FormCasamentoComponent', () => {
  let component: FormCasamentoComponent;
  let fixture: ComponentFixture<FormCasamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormCasamentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCasamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
