import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormCasamentoComponent } from './form.casamento.component';

const routes: Routes = [
  {path: '', component: FormCasamentoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormRoutingModule { }
