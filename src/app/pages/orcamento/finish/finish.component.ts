import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.scss']
})
export class FinishComponent implements OnInit {

  public today
  public codigo

  constructor(private route: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.codigo = this.activatedRoute.snapshot.params.id
    this.today = new Date()
  }


  sendHome() {
    this.route.navigate(['/orcamento'])
  }


}
