import { CommonModule, registerLocaleData } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TextMaskModule } from 'angular2-text-mask';
import { CurrencyMaskConfig, CurrencyMaskModule, CURRENCY_MASK_CONFIG } from "ng2-currency-mask";
import { NgxMaskModule } from 'ngx-mask';
import { EnviarComponent } from './components/enviar/enviar.component';
import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form.component';
import localePt from '@angular/common/locales/pt';
import { DownloadComponent } from './components/download/download.component';
registerLocaleData(localePt, 'pt');

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "right",
  allowNegative: false,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: "."
};

@NgModule({
  declarations: [FormComponent, EnviarComponent, DownloadComponent],
  imports: [
    CommonModule,
    FormRoutingModule,
    CurrencyMaskModule,
    MatButtonModule,
    NgxMaskModule.forRoot(),
    FormsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    TextMaskModule
  ],
  providers: [
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    {
      provide: LOCALE_ID,
      useValue: 'pt'
    },
  ]
})
export class FormModule { }
