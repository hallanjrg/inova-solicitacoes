import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ApiService } from 'src/app/services/api.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import validator from 'validar-telefone';

@Component({
  selector: 'app-enviar',
  templateUrl: './enviar.component.html',
  styleUrls: ['./enviar.component.scss']
})
export class EnviarComponent implements OnInit {

  public baixarOrcamento: FormGroup
  public toast;
  public blob;
  public error = {
    phone: {
      message: ''
    },
    email: {
      message: ''
    }
  }

  constructor(private bsModal: BsModalService,
    private toastService: ToastService,
    private router: Router,
    private fb: FormBuilder,
    private apiService: ApiService) {
    this.baixarOrcamento = this.fb.group({
      codigo: [''],
      email: [''],
      nome: [''],
      telefone: ['']
    })
  }

  ngOnInit(): void {
    this.baixarOrcamento.get('codigo').setValue(this.bsModal.config['code'])
  }

  close() {
    this.bsModal.hide()
  }

  phoneNumberIsReal(data) {
    if (data.length >= 10) {
      if (!validator(data)) {
        this.error.phone.message = 'O telefone digitado não é válido.'
        this.baixarOrcamento.get('telefone').setErrors({ 'invalid': true })
      } else {
        this.error.phone.message = ''
      }
    } else {
      this.error.phone.message = 'Este campo é obrigatório.'
    }
  }

  validacaoEmail(field: string) {
    if (field) {
      let usuario = field.substring(0, field.indexOf("@"));
      let dominio = field.substring(field.indexOf("@") + 1, field.length);
      if ((usuario.length >= 1) &&
        (dominio.length >= 3) &&
        (usuario.search("@") == -1) &&
        (dominio.search("@") == -1) &&
        (usuario.search(" ") == -1) &&
        (dominio.search(" ") == -1) &&
        (dominio.search(".") != -1) &&
        (dominio.indexOf(".") >= 1) &&
        (dominio.lastIndexOf(".") < dominio.length - 1)) {
        this.error.email.message = ''
        this.baixarOrcamento.get('email').setErrors(null)
      }
      else {
        this.error.email.message = 'O e-mail digitado não é válido.'
        this.baixarOrcamento.get('email').setErrors({ 'invalid': true })
      }
    }
  }

  validate(){
    this.toastService.remove(this.toast)
    let errors = []
       let b = this.baixarOrcamento.controls
       if (b) {
         for (let name in b) {
           if (b[name].invalid) {
             b[name].markAsTouched()
             errors.push(name)
           }
         }
       }

     if (errors.length > 0) {
       this.toast = this.toastService.show(`Os campos circulados em vermelho são obrigatórios.`,
         { classname: 'text-center toast-style', delay: 5000 });
       return false
     } else {
        this.submit();
     }
  }

  submit() {
      this.apiService.baixarOrcamentoPdf(`orcamento/baixarorcamento?codigo=${this.baixarOrcamento.value.codigo}&email=${this.baixarOrcamento.value.email}&telefone=${this.baixarOrcamento.value.telefone}&nome=${this.baixarOrcamento.value.nome}`).subscribe((data: any) => {
        this.blob = new Blob([data], {type: 'application/pdf'});
        var downloadURL = window.URL.createObjectURL(data);
        var link = document.createElement('a');
        link.href = downloadURL;
        link.download = "orçamento.pdf";
        this.bsModal.hide()
        link.click();
      })
  }

}
