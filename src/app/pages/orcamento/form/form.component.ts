import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { faDownload, faPaperPlane, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import html2canvas from 'html2canvas';
// import jspdf from 'jspdf';
import { Orcamento, ResultOrcamento } from 'src/app/models/orcamento';
import { Token } from 'src/app/models/token-interface';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ModalService } from 'src/app/services/modal.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import { EnviarComponent } from './components/enviar/enviar.component';
import { Title } from '@angular/platform-browser';
import { AngularFaviconService } from 'angular-favicon';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  public icons = {
    faTrashAlt,
    faDownload,
    faPaperPlane
  }

  public download = false
  public urlLogo: any;
  public screenRes = window.screen.width
  public orcamento: FormGroup;
  public result: Array<ResultOrcamento>
  public tipos
  public hasResults = false
  public toast
  public blob;
  public menu: any;

  constructor(private formBuilder: FormBuilder,
    private modal: ModalService,
    private toastService: ToastService,
    private apiService: ApiService,
    public loadService: LoadingService,
    public route: ActivatedRoute,
    private favIcon: AngularFaviconService,
    private t: Title) {
    this.t.setTitle('Orçamento');
    this.menu =  this.route.snapshot.queryParams['m']
    this.orcamento = this.formBuilder.group({
      ...Orcamento.novo(this.orcamento)
    });
  }

  ngOnInit() {
    this.screenRes = window.screen.width
    this.apiService.getToken('token', '').subscribe((res: Token) => {
      localStorage.setItem('token', res.token);
      this.getCartorio()
      this.getPercentual()
    })
  }

  getCartorio(){
    this.apiService.getApi(`cartorio/get`).subscribe((res: any) => {
      this.urlLogo = res.urlLogo
      this.favIcon.setFavicon(this.urlLogo);
    })
  }

  getPercentual() {
    this.apiService.getApi(`orcamento/percentuaisorcamento`).subscribe(res => {
      this.tipos = res
    })
  }

  getT(type: string) {
    if (this.tipos?.length > 0) {
      let t = this.tipos.find(t => t.tipoEscritura == type)
      return t?.percentual
    }
  }

  baixarOrcamento() {
    this.modal.open(EnviarComponent, {
      code: this.result[0].codigoSimples
    })
    this.modal.modalRef.onHidden.subscribe(() => {
    })
    /* let code = this.result[0].codigo;

    this.apiService.baixarOrcamentoPdf(`orcamento/baixarorcamento?codigo=${code}`).subscribe((data: any) => {

      this.blob = new Blob([data], {type: 'application/pdf'});

      var downloadURL = window.URL.createObjectURL(data);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = "orçamento.pdf";
      link.click();
    }) */



    // this.loadService.loadButtonActive = true
    // this.download = true
    // this.toastService.remove(this.toast)
    // setTimeout(() => {
    //   this.download = false
    //   let element = document.getElementById('content')
    //   html2canvas(element).then((canvas) => {
    //     let imageData = canvas.toDataURL('image/png')
    //     let doc = new jspdf()
    //     let imageHeight = canvas.height * 208 / canvas.width
    //     doc.addImage(imageData, 0, 0, 208, imageHeight)
    //     doc.save(`Orçamento`)
    //   }).then(() => {
    //     this.loadService.loadButtonActive = false
    //     this.toast = this.toastService.show('Download concluído',
    //       { classname: 'text-center toast-style success', delay: 5000 })
    //   })
    // }, 1);
  }

  calcular() {
    this.toastService.remove(this.toast)
    if (this.validateForm()) {
      this.apiService.postApi(`orcamento/calcular`, this.orcamento.value).subscribe((res: ResultOrcamento) => {
        this.hasResults = true
        this.result = [res]
      }, err => this.toast = this.toastService.show('Algo deu errado com o cálculo deste orçamento', {
        classname: 'text-center danger'
      }))
    }

  }

  validateForm() {
    let controls = this.orcamento.controls
    for (let name in controls) {
      if (name == 'valor' && controls[name].value == 0) {
        this.toast = this.toastService.show('O valor não pode ser R$0,00',
          { classname: 'text-center toast-style danger danger', delay: 5000 })
        return false
      }
      if (controls[name].invalid) {
        controls[name].markAsTouched()
        this.toast = this.toastService.show('Campos em vermelho inválidos',
          { classname: 'text-center toast-style danger', delay: 5000 })
        return false
      }
    }
    return true;
  }

}
