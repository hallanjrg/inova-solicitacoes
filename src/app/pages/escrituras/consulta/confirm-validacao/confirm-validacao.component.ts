import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { InfoService } from 'src/app/services/info.service';

@Component({
  selector: 'app-confirm-validacao',
  templateUrl: './confirm-validacao.component.html',
  styleUrls: ['./confirm-validacao.component.scss']
})
export class ConfirmValidacaoComponent implements OnInit {
  public motivo = '';

  constructor(
    public dialogRef: MatDialogRef<ConfirmValidacaoComponent>,
    private apiService: ApiService,
    public infoService: InfoService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}

  ngOnInit(): void {
  }

  close(){
    this.dialogRef.close()
  }

  submit(aprovado: any){
    if(aprovado == true){
      const json = {
        codigoInterno: this.infoService.arraySolicitacoes.codigo,
        codigo: this.data.doc.codigo,
        valido: this.data.validation,
      }
        this.apiService.postApi(`solicitacaocertidao/aprovar-documento`, json).subscribe(res => {
          this.close()
        })
    }else{
      const json = {
        codigoInterno: this.infoService.arraySolicitacoes.codigo,
        codigo: this.data.doc.codigo,
        valido: this.data.validation,
        obs: this.motivo
      }
      this.apiService.postApi(`solicitacaocertidao/aprovar-documento`, json).subscribe(res => {
        this.close()
      })
    }
  }

}
