import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { InfoService } from 'src/app/services/info.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ProvidenciasComponent } from '../components/providencias/providencias.component';

@Component({
  selector: 'app-historico-pendencia',
  templateUrl: './historico-pendencia.component.html',
  styleUrls: ['./historico-pendencia.component.scss']
})
export class HistoricoPendenciaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<ProvidenciasComponent>,
    public loadingService: LoadingService,
    private apiService: ApiService,
    public infoService: InfoService,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit(): void {
    let messageBody = document.querySelector('#modal-body-id')
    if (messageBody) {
      setTimeout(() => {
        messageBody!.scrollTop = messageBody!.scrollHeight - messageBody!.clientHeight
      }, 1);
    }
  }

  baixarArquivo(link:any){
    window.open(link)
  }

  close(){
    this.dialogRef.close()
  }

}
