import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { InfoService } from 'src/app/services/info.service';
import { LoadingService } from 'src/app/services/loading.service';
import { FileType } from 'src/app/models/type';
import { ProvidenciasService } from 'src/app/services/providencias.service';

@Component({
  selector: 'app-providencias',
  templateUrl: './providencias.component.html',
  styleUrls: ['./providencias.component.scss']
})
export class ProvidenciasComponent implements OnInit {
  public arquivo: Array<string> = [];
  public files: Array<FileType> = [];
  public message: String = ''
  constructor(
    public dialogRef: MatDialogRef<ProvidenciasComponent>,
    public loadingService: LoadingService,
    private apiService: ApiService,
    public infoService: InfoService,
    public providenciasService: ProvidenciasService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
    console.log(this.data)
  }

  ngOnInit(): void {
  }


  sendPendingMessage() {
      const message = {
        arquivo: this.arquivo,
        codigoSolicitacao: this.data.providencia.codigoSolicitacao,
        codigoPendencia: this.data.providencia.codigo,
        mensagem: this.message
      }
      this.apiService.postApi(`chat/enviar`, message).subscribe(res => {
        this.solve()
      })
}

solve() {
  this.data.providencia.resolvido = null
  this.apiService.postApi(`pendencia/save`, this.data.providencia).subscribe(res => {
    this.dialogRef.close()
  })
}

  handleSelect(event: any) {
    this.loadingService.isActive = true
    let array = Array.from(event.target.files)
    array.forEach(element => {
      this.upload(element)
    })
    event.target.value = ''
  }


  upload(fileList: any) {
    this.loadingService.isActive = true
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.apiService.postApiUpload('bucket/upload', formData).subscribe((res: any) => {
      this.loadingService.isActive = false
      this.files.push({ nome: fileList.name })
      this.arquivo.push(res.codigoArquivo)
    }, err => {
      this.loadingService.isActive = false
    })
  }

  deleteFile(index) {
    this.files.splice(index, 1)
      this.arquivo.splice(index, 1)
  }




}
