import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidenciasComponent } from './providencias.component';

describe('ProvidenciasComponent', () => {
  let component: ProvidenciasComponent;
  let fixture: ComponentFixture<ProvidenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
