import { HistoricoPendenciaComponent } from './historico-pendencia/historico-pendencia.component';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { InfoService } from 'src/app/services/info.service';
import { TokenService } from 'src/app/services/token.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ProvidenciasComponent } from './components/providencias/providencias.component';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { ProvidenciasService } from 'src/app/services/providencias.service';
import { ActivatedRoute } from '@angular/router';
import { Cartorio } from 'src/app/models/cartorio';
import { Token } from 'src/app/models/token-interface';
import { ConfirmValidacaoComponent } from './confirm-validacao/confirm-validacao.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})

export class ConsultaComponent implements OnInit {
  @Input() messages: any
  public arquivos: Array<any> = []
  public filesShow: Array<any> = [];
  public form: FormGroup
  public message: string = ''
  public files = [];
  constructor(public infoService: InfoService,
    public tokenService: TokenService,
    public loadingService: LoadingService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private fb: FormBuilder,
    public apiService: ApiService,
    public providenciasService: ProvidenciasService,
  ) {
    this.form = this.fb.group({
      codigoSolicitacao: ['', Validators.required],
      mensagem: [''],
      arquivo: ['']
    })
  }

  ngOnInit(): void {
    this.init()

  }

  checkMessages() {
    if (this.infoService?.messages.length > 0) {
      return true
    }
  }

  scrollToBottom() {
    let messageBody = document.querySelector('#collapseChat')
    if (messageBody) {
      setTimeout(() => {
        messageBody!.scrollTop = messageBody!.scrollHeight - messageBody!.clientHeight
      }, 1);
    }
  }

  init() {
    let c = this.route.snapshot.params.id
    this.apiService.getToken(`token`, c).subscribe((res: Token) => {
      localStorage.setItem('token', res.token);
      this.apiService.getCartorioAtual(`cartorio/get`, c).subscribe((res: Cartorio) => {
        this.apiService.setCartorio(res)
      })
      this.providenciasService.hideBadge = true
      this.infoService.base64 = []
      this.infoService.buscarSolicitacao = false
      this.providenciasService.atualizarTela.subscribe(res => {
        if (res) {
          this.infoService.consultar(this.infoService.codigo_solicitacao)
          this.infoService.base64 = []
        }
      })
    })
  }

  isCPF(cpfCnpj): string {
    return cpfCnpj && cpfCnpj.length < 12 ? '000.000.000-009' : '00.000.000/0000-00';
  }

  getPendingQtd(arr: Array<any>) {
    let r = 0
    if (Array.isArray(arr)) {
      arr.forEach(element => {
        if (element.resolvido === false) {
          r++
        }
      });
      if (r > 0) this.providenciasService.hideBadge = false
      return r
    }
  }

  getPendingQtdDocumentos(arr: Array<any>) {
    let r = 0
    if (Array.isArray(arr)) {
      arr.forEach(element => {
        if (element.aprovado === null) {
          r++
        }
      });
      if (r > 0) this.providenciasService.hideBadge = false
      return r
    }
  }

  getStatus(tipo: any) {
    if (tipo == null) {
      return 'Enviado';
    } else if (tipo == true) {
      return 'Aprovado';
    } else {
      return 'Reprovado';
    }
  }

  download(doc: any) {
    doc.documentos.forEach((element: any) => {
      window.open(element, '_blank');
    });
  }

  getAssunto(tipo: string) {
    switch (tipo) {
      case 'MINUTA_PREVIA':
        return 'Minuta prévia';
        break;
      case 'ORCAMENTO':
        return 'Orçamento';
        break;
      default:
        return 'Outros';
        break;
    }
  }


  handleFileChange(event) {
    const target = event.target
    const { files } = target /* ====  const files = target.files */
    for (let element of files) {
      if (files && files[0]) {
        const reader = new FileReader();
        reader.onload = event => {
          const dataUri = event.target.result as string;
          const base64 = dataUri.replace(/^data:.+;base64,/, '')
          this.infoService.base64.push({
            nome_arquivo: element.name.toLowerCase(),
            base64 /* == base64: base64 */
          })
        }
        reader.readAsDataURL(element)
      }
    }
  }


  handleSelect(event: any) {
    this.loadingService.isActive = true
    let array = Array.from(event.target.files)
    array.forEach(element => {
      this.upload(element)
    })
    event.target.value = ''
  }

  upload(fileList: any) {
    this.loadingService.isActive = true
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.apiService.postApiUpload('bucket/upload', formData).subscribe((res: any) => {
      this.loadingService.isActive = false
      this.infoService.base64.push({
        nome_arquivo: fileList.name.toLowerCase(),
        base64: '' /* == base64: base64 */
      })
      this.files.push(res.codigoArquivo)
      this.form.get('arquivo').setValue(this.files)
    }, err => {
      this.loadingService.isActive = false
    })
  }

  removeSelectedFile(file) {
    const index = this.infoService.base64.indexOf(file)
    this.infoService.base64.splice(index, 1);
  }

  removeSelectedIndex(index){
    this.arquivos.splice(index,1)
    this.filesShow.splice(index,1)
  }

  get cpfCnpj() {
    return this.infoService.cpfSolicitacao
  }


  resolver(providencia) {
    this.providenciasService.providenciaSelecionada = providencia
    const dialogRef = this.dialog.open(ProvidenciasComponent, {
      width: '600px',
      data: { ...this.infoService.arraySolicitacoes, providencia }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.infoService.consultar(this.infoService?.codigo_solicitacao)
    });


  }

  openHistory(providencia: any) {
    const dialogRef = this.dialog.open(HistoricoPendenciaComponent, {
      data: providencia,
      width: '600px',
    })
  }

  docValidation(doc: any, validation: boolean) {
    const dialogRef = this.dialog.open(ConfirmValidacaoComponent, {
      data: { doc, validation },
      width: '800px',
    })
    dialogRef.afterClosed().subscribe(result => {
      this.infoService.consultar(this.infoService?.codigo_solicitacao)
    });
  }



  sendMessage() {
    if(this.form.value.arquivo == ""){
      let mensagem = [];
      this.form.get('arquivo')?.setValue(mensagem)
    }
    this.form = this.fb.group({
      codigoSolicitacao: [this.infoService?.arraySolicitacoes.codigo],
      mensagem: [this.form.value.mensagem],
      arquivo: [this.arquivos]
    })
    this.apiService.postApi(`chat/enviar`, this.form.value).subscribe(res => {
      this.loadMessage()
    })
  }

  downloads(link: string) {
    window.open(link, '_blank')
  }


  loadMessage() {
    this.form = this.fb.group({
      codigoSolicitacao: [this.infoService?.arraySolicitacoes.codigo],
      mensagem: [''],
      arquivo: ['']
    })
    this.removeFile()
    this.infoService.consultar(this.infoService.codigo_solicitacao)
  }

  handleSelectChat(event: any) {
    let array = Array.from(event.target.files)
    array.forEach(element => {
      this.uploadChat(element)
    })
    event.target.value = ''
  }

  uploadChat(fileList: any) {
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.apiService.postApiUpload('bucket/upload', formData).subscribe((res: any) => {
      this.arquivos.push(res.codigoArquivo)
      this.filesShow.push({ nome: fileList.name })
      this.form.get('arquivo')?.setValue(this.arquivos)
    }, err => {
    })
  }

  removeFile() {
    this.arquivos = []
    this.filesShow = []
    this.form.get('arquivo')?.setValue('')
  }

}

