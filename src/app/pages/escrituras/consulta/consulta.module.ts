import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultaRoutingModule } from './consulta-routing.module';
import { ConsultaComponent } from './consulta.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { TextMaskModule } from 'angular2-text-mask';
import { ProvidenciasComponent } from './components/providencias/providencias.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatBadgeModule } from '@angular/material/badge';
import { HistoricoPendenciaComponent } from './historico-pendencia/historico-pendencia.component';
import { ConfirmValidacaoComponent } from './confirm-validacao/confirm-validacao.component';


@NgModule({
  declarations: [
    ConsultaComponent,
    ProvidenciasComponent,
    HistoricoPendenciaComponent,
    ConfirmValidacaoComponent
  ],
  imports: [
    CommonModule,
    ConsultaRoutingModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCardModule,
    FormsModule,
    NgxMaskModule.forRoot(),
    TextMaskModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatTabsModule,
    MatBadgeModule
  ]
})
export class ConsultaModule { }
