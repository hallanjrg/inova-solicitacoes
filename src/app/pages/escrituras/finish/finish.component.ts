import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InfoService } from 'src/app/services/info.service';
import { TokenService } from 'src/app/services/token.service';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';

interface tokenInterface {
  access_token: string;
  expires_in: number;
  token_type: string;
}

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.scss']
})
export class FinishComponent implements OnInit {
  public codigoCartorio: any;
  codigo
  today

  constructor(private route: Router,
    private activatedRoute: ActivatedRoute,
    public infoService: InfoService,
    private router: ActivatedRoute) {
    this.codigoCartorio = this.router.snapshot.queryParams.codigoCartorio;
  }


  ngOnInit(): void {
    this.codigo = this.activatedRoute.snapshot.params.id
    this.today = new Date()
  }

  backConsulta() {
    this.route.navigate([`/certidoes/consulta/${this.codigoCartorio}`], {});
  }

  sendHome() {
    this.infoService.base64 = []
    this.route.navigate(['/escrituras'])
  }

}
