import { Component, Inject, OnInit } from '@angular/core';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InfoService } from 'src/app/services/info.service';


@Component({
  selector: 'app-confirm-modal-certificate',
  templateUrl: './confirm-modal-certificate.component.html',
  styleUrls: ['./confirm-modal-certificate.component.scss']
})
export class ConfirmModalCertificateComponent implements OnInit {

  constructor(
    public infoService: InfoService,
    public dialogRef: MatDialogRef<ConfirmModalCertificateComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit() {
  }

  close(condition: boolean): void {
    this.dialogRef.close(condition);
  }




}
