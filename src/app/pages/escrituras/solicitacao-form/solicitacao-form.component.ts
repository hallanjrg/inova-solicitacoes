import { buscaCep } from './../../../models/busca-cep';
import { Token, tokenInterface } from './../../../models/token-interface';
import { Component, OnInit, ChangeDetectorRef, Inject } from '@angular/core';
import { InfoService } from '../../../services/info.service';
import { TokenService } from '../../../services/token.service';

import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { LoadingService } from 'src/app/services/loading.service';
import { ApiService } from 'src/app/services/api.service';
import { BuscaCepService } from 'src/app/services/busca-cep.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { cnpj, cpf } from 'cpf-cnpj-validator';
import validator from "validar-telefone";
import { CepService } from 'src/app/services/cep.service'
import { NewSolicitacao } from 'src/app/models/solicitacao'
import * as moment from 'moment';
import { ConfirmModalCertificateComponent } from './confirm-modal-certificate/confirm-modal-certificate.component';
import { LocateService } from 'src/app/services/locate.service';
import { ActivatedRoute } from '@angular/router';
import { Cartorio } from 'src/app/models/cartorio';
import { Location } from '@angular/common';
import { timeout } from 'rxjs/operators';

@Component({
  selector: 'app-solicitacao-form',
  templateUrl: './solicitacao-form.component.html',
  styleUrls: ['./solicitacao-form.component.scss']
})

export class SolicitacaoFormComponent implements OnInit {
  public estados
  public valorCertidao = 0;
  public valorAdicional: any;
  public toGo: NewSolicitacao = new NewSolicitacao()
  public novaSolicitacao
  public checkEnderecoEntrega = false;
  public listaResponsavel = [];
  public error = {
    cpfCnpj: {
      message: ''
    },
    phone: {
      message: ''
    },
    email: {
      message: ''
    }
  }
  profileForm = this.fb.group({
    certidaoDigital: [false],
    dados1: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ), dados2: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ), dados3: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ), dados4: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ), dados5: this.fb.group({
      nomePartes: (''),
      dataAto: (''),
      tipoAto: (''),
      termo: (''),
      livro: (''),
      folha: (''),
    }
    ),
    entrega: [0],
    endereco: this.fb.group({
      cep: [''],
      logradouro: [''],
      numero: [''],
      complemento: [''],
      bairro: [''],
      cidade: [''],
      uf: [''],
      valorFrete: [0],
      retirar: [0]
    }), enderecoSolicitante: this.fb.group({
      cep: [''],
      logradouro: [''],
      numero: [''],
      complemento: [''],
      bairro: [''],
      cidade: [''],
      uf: [''],
    }),
    solicitante: this.fb.group({
      nome: [''],
      cpfCnpj: [''],
      responsavel: [''],
      email: ['', ([Validators.required, Validators.email])],
      telefone: [''],
    }),
    mensagem: [''],
  })

  constructor(private fb: FormBuilder,
    public infoService: InfoService,
    private cdr: ChangeDetectorRef,
    public tokenService: TokenService,
    public loadingService: LoadingService,
    public apiService: ApiService,
    public buscaCepService: BuscaCepService,
    @Inject(MAT_DIALOG_DATA) public data,
    public dialog: MatDialog,
    private cepService: CepService,
    public locateService: LocateService,
    private route: ActivatedRoute,
    public location: Location
  ) {
    this.toGo = new NewSolicitacao()
  }

  ngOnInit(): void {
    let c = this.route.snapshot.queryParams['codigoCartorio']
    this.apiService.getToken(`token`, c).subscribe((res: Token) => {
      localStorage.setItem('token', res.token);
      this.apiService.getCartorioAtual(`cartorio/get`, c).subscribe((res: Cartorio) => {
        this.apiService.setCartorio(res);
        this.addValorCartorio(res.codigo);
      })
      this.locateService.getEstados()
      this.listarResponsavel();
    })
    /*  this.apiService.getCartorio(window.location.href) */


  }

  addValorCartorio(codigoCartorio) {
    this.apiService.getApi(`cartorio/obter-configuracoes?codigoCartorio=${codigoCartorio}`).subscribe((res: any) => {
      this.valorAdicional = res.valorAdicional;
      let obter = res.valoresSolicitacoes
      obter.forEach(element => {
        if (element.tipoSolicitacao == 'CERTIDAO_ESCRITURA') {
          let i = 0
          this.valorCertidao = element.tipoValor[i].valor
        }
      });
    })
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  phoneNumberIsReal(data) {
    if (data.length >= 10) {
      if (!validator(data)) {
        this.error.phone.message = 'O telefone digitado não é válido.'
        this.profileForm.get('solicitante').get('telefone').setErrors({ 'invalid': true })
      } else {
        this.error.phone.message = ''
      }
    }
  }

  validacaoEmail(field: string) {
    if (field) {
      field = field.toLocaleLowerCase()
      let usuario = field.substring(0, field.indexOf("@"));
      let dominio = field.substring(field.indexOf("@") + 1, field.length);
      if ((usuario.length >= 1) &&
        (dominio.length >= 3) &&
        (usuario.search("@") == -1) &&
        (dominio.search("@") == -1) &&
        (usuario.search(" ") == -1) &&
        (dominio.search(" ") == -1) &&
        (dominio.search(".") != -1) &&
        (dominio.indexOf(".") >= 1) &&
        (dominio.lastIndexOf(".") < dominio.length - 1)) {
        this.error.email.message = ''
        this.profileForm.get('solicitante').get('email').setErrors(null)
        this.profileForm.value.solicitante.email = field
      }
      else {
        this.error.email.message = 'O e-mail digitado não é válido.'
        this.profileForm.get('solicitante').get('email').setErrors({ 'invalid': true })
      }
    }
  }



  cpfCnpjIsReal(data) {
    if (data.length > 11) {
      if (!cnpj.isValid(data)) {
        this.error.cpfCnpj.message = 'O CNPJ digitado não é válido.'
        this.profileForm.get('solicitante').get('cpfCnpj').setErrors({ 'invalid': true })
      } else {
        this.error.cpfCnpj.message = ''
      }
    } else if (data.length == 11) {
      if (!cpf.isValid(data)) {
        this.error.cpfCnpj.message = 'O CPF digitado não é válido.'
        this.profileForm.get('solicitante').get('cpfCnpj').setErrors({ 'invalid': true })
      } else {
        this.error.cpfCnpj.message = ''
      }
    }
    else {
      this.error.cpfCnpj.message = 'Este campo é obrigatório.'
    }
  }

  get phoneMask() {
    return this.isPhone() ? '(00) 0000-00009' : '(00) 00000-0000'
  }

  isPhone() {
    return this.profileForm.get('solicitante').get('telefone').value.length <= 10
  }

  public cpfcnpjmask = function (rawValue) {
    let numbers = rawValue.match(/\d/g);
    let numberLength = 0;
    if (numbers) {
      numberLength = numbers.join('').length;
    }
    if (numberLength <= 11) {
      return [/[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    } else {
      return [/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
    }
  }

  addFields() {
    if (this.buscaCepService.qtdSolicitacoes < 6) {
      this.buscaCepService.qtdSolicitacoes += 1
      this.buscaCepService.valorServico = this.valorCertidao * this.buscaCepService.qtdSolicitacoes
    }
  }

  removeField() {
    if (this.buscaCepService.qtdSolicitacoes > 0) {
      this.buscaCepService.qtdSolicitacoes -= 1
      this.buscaCepService.valorServico = this.valorCertidao * this.buscaCepService.qtdSolicitacoes
    }
  }

  listarResponsavel() {
    this.apiService.getApi('usuario/listar-recebedosite').subscribe((r: Array<any>) => {
      this.listaResponsavel = r;
    })
  }

  getAddressOnly(params) {
    if (params.length === 8) {
      this.cepService.setAddress(params).subscribe((res: buscaCep) => {
        this.profileForm.get('enderecoSolicitante').get('logradouro').setValue(res.logradouro)
        this.profileForm.get('enderecoSolicitante').get('bairro').setValue(res.bairro)
        this.profileForm.get('enderecoSolicitante').get('uf').setValue(res.uf)
        if (this.profileForm.controls.enderecoSolicitante.value.uf != "") {
          this.locateService.getMunicipiosPorEstado(this.profileForm.controls.enderecoSolicitante.value.uf)
        }
        this.profileForm.get('enderecoSolicitante').get('cidade').setValue(res.localidade)
      })
    }
  }

  getCep(params, tipoEndereco) {
    if (params.length == 8) {
      this.apiService.getApi(`correios/calcularfrete?origem=${this.apiService.chosenCartorio.cep}&destino=${params}`)
        .pipe(timeout(10000)).subscribe((res1: any) => {
          this.setNewAddress(params, res1);
        }, err => {
          this.setNewAddress(params);
        })
    }
  }

  setNewAddress(params, res1?) {
    this.cepService.setAddress(params).subscribe((res: buscaCep) => {
      this.profileForm.get('endereco').get('logradouro').setValue(res.logradouro)
      this.profileForm.get('endereco').get('bairro').setValue(res.bairro)
      this.profileForm.get('endereco').get('uf').setValue(res.uf)
      if (this.profileForm.controls.endereco.value.uf != "") {
        this.locateService.getMunicipiosPorEstado(this.profileForm.controls.endereco.value.uf)
      }
      this.profileForm.get('endereco').get('cidade').setValue(res.localidade)
      this.buscaCepService.frete = res1?.valor || 0
      this.buscaCepService.calculaTotal(res1?.valor || 0)
    })
  }


  removeSelectedFile(file) {
    const index = this.infoService.base64.indexOf(file)
    this.infoService.base64.splice(index, 1);
  }
  openConfirmModalCertificate() {
    let dialogRef = this.dialog.open(ConfirmModalCertificateComponent, {
      width: '600px',
      data: this.profileForm.value
    });
    dialogRef.afterClosed().subscribe(res => {
      this.submit(res)
    });
  }

  validateCertificate() {
    if (this.profileForm.value.certidaoDigital == true) {
      this.openConfirmModalCertificate()
    } else {
      this.submit(true)
    }
  }

  getEstados() {
    this.apiService.getApiListaEstados(`dominio/estados`).subscribe((res) => {

      this.estados = Object.entries(res);
      let newEstados = [];
      this.estados.forEach((element) => {
        newEstados.push(element[0]);
      });
      this.estados = newEstados.sort();
    });
  }

  submit(cancelButtonClicked?) {
    this.phoneNumberIsReal(this.profileForm.value.solicitante.telefone)
    this.validacaoEmail(this.profileForm.value.solicitante.email)
    this.cpfCnpjIsReal(this.profileForm.value.solicitante.cpfCnpj)
    this.infoService.summaryCheck = this.profileForm.value
    this.buscaCepService.summary = this.profileForm.value
    this.toGo.dados = []
    for (let index = 0; index < this.buscaCepService.qtdSolicitacoes; index++) {
      const element = this.toGo.dados[index];
      let dado = {
        ...this.profileForm.get(`dados${index + 1}`).value
      }
      if (dado.dataAto) {
        let y = dado.dataAto.substr(0, 4)
        let m = dado.dataAto.substr(5, 2)
        let d = dado.dataAto.substr(8, 2)
        dado.dataAto = moment(new Date(y, m - 1, d)).format('YYYY-MM-DD')
      }
      this.toGo.dados.push(dado)
    }
    this.toGo.certidaoDigital = this.profileForm.value.certidaoDigital
    this.toGo.dadosSolicitante = this.profileForm.get('solicitante').value
    this.toGo.endereco = this.profileForm.get('endereco').value
    this.toGo.enderecoSolicitante = this.profileForm.get('enderecoSolicitante').value
    this.toGo.mensagem = this.profileForm.get('mensagem').value
    this.toGo.tipoSolicitacao = 'CERTIDAO_ESCRITURA'
    this.toGo.tipoRetirada = this.profileForm.get('endereco').get('retirar').value
    this.toGo.valorFrete = this.buscaCepService.frete
    this.toGo.valorSolicitacao = (this.valorCertidao * this.toGo.dados.length)
    if (this.toGo.certidaoDigital) {
      this.toGo.valorSolicitacao = this.toGo.valorSolicitacao + this.valorAdicional
    }
    this.toGo.dadosSolicitante.nome = this.toGo.dadosSolicitante.nome.replaceAll("'", '').replaceAll('.', '').replaceAll('-', '')
    this.toGo.arquivos = this.infoService.files
    let a = []
    this.toGo.arquivos.forEach(element => {
      a.push(element.codigoArquivo)
    });
    this.toGo.arquivos = a
    if (cancelButtonClicked && this.profileForm.controls.solicitante.valid) {
      this.openConfirm();
    }

  }

  openConfirm() {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '600px',
      data: this.toGo
    });
  }

  checkedEndereco(enderecoSolicitante) {
    if (this.checkEnderecoEntrega == false) {
      this.profileForm.get('enderecoSolicitante').get('logradouro').setValue(enderecoSolicitante.value.logradouro)
      this.profileForm.get('enderecoSolicitante').get('numero').setValue(enderecoSolicitante.value.numero)
      this.profileForm.get('enderecoSolicitante').get('cep').setValue(enderecoSolicitante.value.cep)
      this.profileForm.get('enderecoSolicitante').get('complemento').setValue(enderecoSolicitante.value.complemento)
      this.profileForm.get('enderecoSolicitante').get('bairro').setValue(enderecoSolicitante.value.bairro)
      this.profileForm.get('enderecoSolicitante').get('cidade').setValue(enderecoSolicitante.value.cidade)
      this.profileForm.get('enderecoSolicitante').get('uf').setValue(enderecoSolicitante.value.uf)
      this.checkEnderecoEntrega = true
    } else {
      this.profileForm.get('enderecoSolicitante').get('logradouro').setValue(null)
      this.profileForm.get('enderecoSolicitante').get('numero').setValue(null)
      this.profileForm.get('enderecoSolicitante').get('cep').setValue(null)
      this.profileForm.get('enderecoSolicitante').get('complemento').setValue(null)
      this.profileForm.get('enderecoSolicitante').get('bairro').setValue(null)
      this.profileForm.get('enderecoSolicitante').get('cidade').setValue(null)
      this.profileForm.get('enderecoSolicitante').get('uf').setValue(null)
      this.checkEnderecoEntrega = false
    }

  }

  onClick(info) {
    if (info === 'Entregar no endereço') {
      this.profileForm.get('endereco').get('logradouro').setValidators([Validators.required])
      this.profileForm.get('endereco').get('numero').setValidators([Validators.required])
      this.profileForm.get('endereco').get('bairro').setValidators([Validators.required])
      this.profileForm.get('endereco').get('cidade').setValidators([Validators.required])
      this.profileForm.get('endereco').get('uf').setValidators([Validators.required])
      this.profileForm.get('endereco').get('retirar').setValue(1)
      this.infoService.forma_entrega = 1
    } else {
      this.profileForm.get('endereco').get('cep').clearValidators();
      this.profileForm.get('endereco').get('logradouro').clearValidators();
      this.profileForm.get('endereco').get('numero').clearValidators();
      this.profileForm.get('endereco').get('bairro').clearValidators();
      this.profileForm.get('endereco').get('cidade').clearValidators();
      this.profileForm.get('endereco').get('uf').clearValidators();
      this.profileForm.get('endereco').get('cep').setValue('')
      this.infoService.opcaoEntregaSelecionada = 'Retirar no cartório'
      this.buscaCepService.calculaTotal(0)
      this.buscaCepService.frete = 0
      this.profileForm.get('endereco').get('cep').updateValueAndValidity();
      this.profileForm.get('endereco').get('logradouro').updateValueAndValidity();
      this.profileForm.get('endereco').get('numero').updateValueAndValidity();
      this.profileForm.get('endereco').get('bairro').updateValueAndValidity();
      this.profileForm.get('endereco').get('cidade').updateValueAndValidity();
      this.profileForm.get('endereco').get('uf').updateValueAndValidity();
      this.profileForm.get('endereco').get('retirar').setValue(0)
      this.infoService.forma_entrega = 0
    }
  }


}
