import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CertidoesComponent } from './certidoes.component';

const routes: Routes = [
  { path: '', component: CertidoesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertidoesRoutingModule { }
