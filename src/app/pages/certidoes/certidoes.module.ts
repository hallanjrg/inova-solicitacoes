import { CertidoesRoutingModule } from './certidoes-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertidoesComponent } from './certidoes.component';

@NgModule({
  imports: [
    CommonModule,
    CertidoesRoutingModule
  ],
  declarations: [CertidoesComponent]
})
export class CertidoesModule { }
