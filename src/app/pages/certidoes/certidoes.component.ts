import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-certidoes',
  templateUrl: './certidoes.component.html',
  styleUrls: ['./certidoes.component.scss'],
})
export class CertidoesComponent implements OnInit {
  public codigoCartorio: any;
  public tipoServico = [
    {
      descricaoFuncionalidade: 'Nascimento',
    },
    {
      descricaoFuncionalidade: 'Casamento',
    },
    {
      descricaoFuncionalidade: 'Óbito',
    },
    {
      descricaoFuncionalidade: 'Escrituras/Procuração',
    },
  ];

  constructor(private router: Router, private route: ActivatedRoute) {
    this.codigoCartorio = this.route.snapshot.params.id;
  }

  ngOnInit() {}

  redirect(descricao) {
    if (descricao == 'Casamento') {
      this.router.navigate([`/casamento`],{
        queryParams: {
          codigoCartorio: this.codigoCartorio,
        },
      })
    } else if (descricao == 'Escrituras/Procuração') {
      this.router.navigate([`/escrituras`], {
        queryParams: {
          codigoCartorio: this.codigoCartorio,
        },
      });
    } else if (descricao == 'Nascimento') {
      this.router.navigate([`/nascimento`], {
        queryParams: {
          codigoCartorio: this.codigoCartorio,
        },
      });
    } else if (descricao == 'Óbito') {
      this.router.navigate([`/obito`], {
        queryParams: {
          codigoCartorio: this.codigoCartorio,
        },
      });
    }
  }
}
