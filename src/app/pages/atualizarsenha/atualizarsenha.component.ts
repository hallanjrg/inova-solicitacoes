import { ToastService } from 'src/app/services/toast/toast-service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-atualizarsenha',
  templateUrl: './atualizarsenha.component.html',
  styleUrls: ['./atualizarsenha.component.scss']
})
export class AtualizarsenhaComponent implements OnInit {
  public email: FormGroup
  public toast: any
  constructor(
    private toastService: ToastService,
    public fb: FormBuilder,
    private router: Router) {
      this.email = this.fb.group({
      email: [null, Validators.required]
    })
    }

  ngOnInit(): void {
  }

  teste(){
    this.toast =this.toastService.removeAll()
    this.toast = this.toastService.show('Senha alterada com sucesso')
  }

  autenticar() {
    localStorage.setItem('token', 'autenticado')
    this.router.navigate(['/'])
  }

}
