import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AtualizarsenhaComponent } from './atualizarsenha.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [AtualizarsenhaComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild([
      {path: '', component: AtualizarsenhaComponent}
    ])
  ]
})
export class AtualizarsenhaModule { }
