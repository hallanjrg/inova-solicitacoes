import { ToastService } from 'src/app/services/toast/toast-service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public login: FormGroup;
  public toast: any;
  public codigo: any;

  constructor(
    private toastService: ToastService,
    private service: LoginService,

    public fb: FormBuilder,
    private router: Router) {
    this.login = this.fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    })
    this.codigo = this.service.loggedUser
  }

  ngOnInit(): void {
  }

  submit() {
    // const { valid, value } = this.login;
    // if (valid) {
    //   this.service.login(value).subscribe(r => {
    //     this.service.getCartorio();
    //     let c = JSON.parse(localStorage.getItem('cartorio')).codigoCartorio
    //     this.router.navigate([`/solicitacao-simples/consulta/${c}`]);
    //   },
    //     (err) => {
    //       this.login.controls.password.reset();
    //     }
    //   );
    // }


  }

}
