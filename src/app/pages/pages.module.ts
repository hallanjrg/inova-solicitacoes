import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from '../components/header/header.component';
import { MenuComponent } from '../components/menu/menu.component';



@NgModule({
  declarations: [PagesComponent, HeaderComponent, MenuComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: 'certidoes/consulta/:id', loadChildren: () => import('./escrituras/consulta/consulta.module').then(m => m.ConsultaModule) },
      { path: 'certidoes/:id', loadChildren: () => import('./certidoes/certidoes.module').then(m => m.CertidoesModule) },
      /* escrituras */
      { path: 'escrituras', loadChildren: () => import('./escrituras/solicitacao-form/solicitacao-form.module').then(m => m.SolicitacaoFormModule) },
      /*  { path: 'consulta', loadChildren: () => import('./escrituras/consulta/consulta.module').then(m => m.ConsultaModule) }, */
      { path: 'escrituras/finish/:id', loadChildren: () => import('./escrituras/finish/finish.module').then(m => m.FinishModule) },
      /*  nascimento*/
      { path: 'nascimento', loadChildren: () => import('./nascimento/form/form.nascimento.module').then(m => m.FormNascimentoModule) },
      { path: 'nascimento/finish/:id', loadChildren: () => import('./nascimento/finish/finish.module').then(m => m.FinishModule) },
      /*  obito */
      { path: 'obito', loadChildren: () => import('./obito/form/form.obito.module').then(m => m.FormObitoModule) },
      { path: 'obito/finish/:id', loadChildren: () => import('./obito/finish/finish.module').then(m => m.FinishModule) },
      /* casamento  */
      { path: 'casamento', loadChildren: () => import('./casamento/form/form.casamento.module').then(m => m.FormModule) },
      { path: 'casamento/finish/:id', loadChildren: () => import('./casamento/finish/finish.module').then(m => m.FinishModule) },
      /*  orcamento */
      { path: 'orcamento', loadChildren: () => import('./orcamento/form/form.module').then(m => m.FormModule) },
      { path: 'orcamento/finish/:id', loadChildren: () => import('./orcamento/finish/finish.module').then(m => m.FinishModule) },
      /*  firma */
      { path: 'firma', loadChildren: () => import('./firma/firma.module').then(m => m.FirmaModule) },
    ])
  ]
})
export class PagesModule { }
