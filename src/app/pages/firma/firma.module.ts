import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask';
import { NgxMaskModule } from 'ngx-mask';
import { FirmaRoutingModule } from './firma-routing.module';
import { FirmaComponent } from './firma.component';

@NgModule({
  declarations: [FirmaComponent],
  imports: [
    CommonModule,
    FirmaRoutingModule,
    NgxMaskModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: []
})
export class FirmaModule { }
