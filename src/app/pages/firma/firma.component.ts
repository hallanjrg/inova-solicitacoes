import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { cnpj, cpf } from 'cpf-cnpj-validator';
import { ConsultaFirma, Firma } from 'src/app/models/firma'
import { ToastService } from 'src/app/services/toast/toast-service';
import { environment } from 'src/environments/environment';

interface Token {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number,
  scope: string;
  user_tenant: string;
  user_id: number;
  user_name: string;
  user_autorizacaoID: number;
  user_fimLiberacao: null;
  user_ibge: number;
  jti: string;
}

@Component({
  selector: 'app-firma',
  templateUrl: './firma.component.html',
  styleUrls: ['./firma.component.scss']
})

export class FirmaComponent implements OnInit {

  public userForm: FormGroup
  public token: string
  public result: Array<ConsultaFirma>
  public hasResults = false
  public toast
  public error = {
    cpf: {
      message: ''
    }
  }

  constructor(private fb: FormBuilder,
    private http: HttpClient,
    private toastService: ToastService) {
    this.userForm = this.fb.group({
      ...Firma.novo('cpf&cpf')
    });
  }

  ngOnInit(): void {
    this.getToken()
  }

  getToken() {
    this.http.post(`${environment.urlFirmaToken}`, null, {
      headers: new HttpHeaders().set('Authorization', `Basic RUlub3ZhLXB1YmxpYzoxNTY3NjI5YjY3Y2U0OTA4YTk1ODRmNTcxYjYzOWExZA==`)
    }).subscribe((res: Token) => {
      this.token = res.access_token
      localStorage.setItem('token', `Bearer ${res.access_token}`)
    })
  }

  submit() {
    this.toastService.remove(this.toast)
    let t = this.userForm.value.tipo
    let n = this.userForm.value.nome
    let c = this.userForm.value.cpf
    if (this.validate()) {
      this.http.get(`${environment.urlFirma}${t}=${t == 'cpf&cpf' ? c : n}`, {
        headers: new HttpHeaders().set('Authorization', localStorage.getItem('token'))
      }).subscribe((res: ConsultaFirma) => {
        this.result = [res]
        if (res) {
          this.toast = this.toastService.show('Cartão assinatura localizado com sucesso!', {
            classname: 'text-center success'
          })
          this.hasResults = true
        } else {
          this.hasResults = false
          this.toast = this.toastService.show('Nenhum registro encontrado', {
            classname: 'text-center danger'
          })
        }
      })
    }
  }

  changeType(t) {
    this.userForm = this.fb.group({
      ...Firma.novo(t)
    });
    this.error.cpf.message = ''
  }

  cpfIsReal(data) {
    if (data.length == 11) {
      if (!cpf.isValid(data)) {
        this.error.cpf.message = 'O CPF digitado não é válido.'
      } else {
        this.error.cpf.message = ''
      }
    } else if (!!data) {
      this.error.cpf.message = 'Este campo é obrigatório.'
    }
  }

  validate() {
    this.toastService.remove(this.toast)
    let c = this.userForm.controls
    if (!!this.error.cpf.message) {
      this.toast = this.toastService.show(`${this.error.cpf.message}`, {
        classname: 'text-center danger'
      })
      return false
    } else if (this.error.cpf.message == '') {
      this.toastService.remove(this.toast)
      for (let name in c) {
        if (c[name].invalid) {
          c[name].markAsTouched()
          this.toast = this.toastService.show('Os campos circulados em vermelho estão inválidos', {
            classname: 'text-center danger'
          })
          return true
        }
      }
    }
    return true
  }

}
