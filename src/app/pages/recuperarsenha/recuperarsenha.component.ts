import { ToastService } from 'src/app/services/toast/toast-service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-recuperarsenha',
  templateUrl: './recuperarsenha.component.html',
  styleUrls: ['./recuperarsenha.component.scss']
})
export class RecuperarsenhaComponent implements OnInit {
  public senha: FormGroup
  public toast: any
  public confirmarsenha: any
  constructor(
    private toastService: ToastService,
    public fb: FormBuilder,
    private router: Router) {
      this.senha = this.fb.group({
      senha:[null, Validators.required],
      confirmarsenha:[null, Validators.required]
    })
    }

  ngOnInit(): void {
  }

  submit() {
    if(this.senha.invalid)  {
      this.toast = this.toastService.show('As senhas não conferem!')
    } else{}

  }

  autenticar() {
    localStorage.setItem('token', 'autenticado')
    this.router.navigate(['/'])
  }
}
