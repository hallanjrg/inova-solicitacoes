import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxMaskModule } from 'ngx-mask';
import { InterceptorService } from './services/interceptor.service';
import { ModalService } from 'src/app/services/modal.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoadingComponent } from './components/loading/loading.component';
import { ConfirmModalComponent } from './pages/escrituras/solicitacao-form/confirm-modal/confirm-modal.component';
import { ConfirmModalCertificateComponent } from './pages/escrituras/solicitacao-form/confirm-modal-certificate/confirm-modal-certificate.component';
import { FinishModule } from './pages/orcamento/finish/finish.module';
import { FormModule } from './pages/orcamento/form/form.module';
import { ToastsContainer } from 'src/app/services/toast/toasts-container.component'
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent,
    ConfirmModalCertificateComponent,
    ConfirmModalComponent,
    ToastsContainer
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormModule,
    FinishModule,
    MatSnackBarModule,
    NgxMaskModule.forRoot(),
    ToastrModule.forRoot(),
    NgbModule,
    NgbModalModule,
    ModalModule.forRoot()
  ],
  providers: [
    ConfirmModalComponent,
    ConfirmModalCertificateComponent,
    ModalService,
    ToastService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
