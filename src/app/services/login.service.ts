import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { UnauthorizedException } from '../exceptions/unauthorized.exception';
import { GenericException } from '../exceptions/generic.exception';
import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private readonly tokenName = 'token';
  private cartorio: any;
  public loggedUser: any;

constructor(private api: ApiService,
  private  router: Router) { }

private save(token: string) {
  localStorage.setItem(this.tokenName, token);
}

public getCartorio() {
  this.cartorio = this.getDecodedAccessToken(localStorage.getItem('token'))
  localStorage.setItem('cartorio', JSON.stringify(this.cartorio))
  this.api.getApi(`/cartorio/getbyid?codigoCartorio=${this.cartorio.codigoCartorio}`).subscribe(res => {
      this.loggedUser = res
  }, err => {
      this.router.navigate(['/login'])
  })
}

private getToken() {
  return localStorage.getItem(this.tokenName);
}

login(body: { email: string, password: string }) {
  return this.api.logIn(body)
      .pipe(
          tap((data) => {
              this.save(data.token)
          }),
          catchError((err) => {
              if (err instanceof HttpErrorResponse) {
                  if (err.status === 401) {
                      throw new UnauthorizedException()
                  }
              }
              throw new GenericException();
          })
      )
}

checkLogged() {
  const token = this.getToken();
  return !!token;
}

getDecodedAccessToken(token: any) {
  try {
      return jwt_decode(token);
  }
  catch (Error) {
      return null
  }
}

}


