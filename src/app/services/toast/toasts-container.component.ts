import { Component, TemplateRef } from '@angular/core';

import { ToastService } from './toast-service';


@Component({
  selector: 'app-toasts',
  template: `
    <div class="d-flex w-100 justify-content-center">
    <ngb-toast
    *ngFor="let toast of toastService.toasts"
    [class]="toast.classname"
    [autohide]="true"
    [delay]="toast.delay || 5000"
    (hidden)="toastService.remove(toast)"
    [header]="toast.header"
    (click)="closeClick(toast)"
    >
      <ng-template [ngIf]="isTemplate(toast)" [ngIfElse]="text">
        <ng-template [ngTemplateOutlet]="toast.textOrTpl"></ng-template>
      </ng-template>

      <ng-template #text>{{ toast.textOrTpl }}</ng-template>
    </ngb-toast>
    </div>
  `,
  host: { '[class.ngb-toasts]': 'true' }
})
export class ToastsContainer {
  constructor(public toastService: ToastService) { }

  isTemplate(toast) { 
    setTimeout(() => {
      this.closeClick(toast)
    }, 5000);
    return toast.textOrTpl instanceof TemplateRef; 
  }

  closeClick(toast) {
    this.toastService.remove(toast);
  }
}
