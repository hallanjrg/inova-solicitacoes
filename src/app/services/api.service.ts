import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { timeout } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { TokenService } from './token.service';
import { ActivatedRoute, UrlTree } from '@angular/router';
import { Cartorio } from '../models/cartorio';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  token = null
  favIcon: HTMLLinkElement = document.querySelector('#appIcon')
  title: Element = document.querySelector('#title')
  cpfPorPagador = ''

  chosenCartorio = {
    favIcon: this.favIcon,
    title: this.title,
    userId: '',
    cep: '',
    urlLogo: ''
  }

  cartorios = [
    {
      id: '390228c0-eef0-11ea-924e-0af504ceb319',
      url: 'https://solicitacoes.cartoriotoledo.com.br/consulta',
      nome: 'Cartório Toledo',
      logo: 'https://inova-cartorios-solicitacoes.s3-us-west-2.amazonaws.com/email_imgs/toledo/logo_toledo.png',
      cep: '05401450',
      locate: 'cartoriotoledo'
    },
    {
      id: '81147fbf-14a0-11eb-8252-0a96c6f995dd',
      url: 'https://solicitacoes.2cartorio.com.br/consulta',
      nome: 'Cartório Tabelião 2',
      logo: 'https://inova-cartorios-solicitacoes.s3-us-west-2.amazonaws.com/email_imgs/2cartorio/logo_2cartorio.png',
      cep: '01220010',
      locate: '2cartorio'
    },
    {
      id: 'f2f01938-37f1-11eb-a93a-0e29607613cd',
      url: 'https://solicitacoes.quintocartorio.com.br/consulta',
      nome: 'Quinto Cartório',
      logo: 'https://inova-cartorios-solicitacoes.s3-us-west-2.amazonaws.com/email_imgs/5TBM/logo.png',
      cep: '04715005',
      locate: 'quintocartorio'
    },
    {
      id: '94dcbdf5-1977-11eb-8252-0a96c6f995dd',
      url: 'https://localhost:3000',
      nome: 'Teste LocalHost',
      logo: 'https://www.google.com/favicon.ico',
      cep: '01220010',
      locate: 'localhost'
    }
  ]

  /* userId = ' 3901e05b-eef0-11ea-924e-0af504ceb319' */  //teste caio

  public httpOptions = {
    headers: new HttpHeaders(
      {
        'Authorization': '',
      }
    )
  };

  public httpOptions2 = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': '',
      }
    ),
  }

  getCartorio(url) {
    this.cartorios.forEach(element => {
      let find = url.search(element.locate)
      if (find > 0) {
        this.chosenCartorio.userId = element.id
        this.favIcon.href = element.logo
        this.title.innerHTML = element.nome
        this.chosenCartorio.cep = element.cep
      }
    })
  }

  logIn(body: { email: string, password: string }) {
    return this.http.post<{ msg: string, dateHourLogout: any, token: string }>(`${environment.url}/auth/login`, body);
  }

  getApiLocate<T>(params: string): Observable<T> {
    return this.http.get<T>(environment.locateUrl + params)
  }

  postFork(params, data) {
    const chamada = data.map(res => {
      return this.http.post(environment.url + params, res, this.httpOptions)
    })
    return forkJoin(chamada)
  }

  constructor(private http: HttpClient,
    public tokenService: TokenService,
    private route: ActivatedRoute) {
  }

  getCepApiAntigo<T>(params): Observable<T> {
    return this.http.get<T>(params)
  }

  getApiAntigo<T>(params): Observable<T> {
    return this.http.get<T>(environment.url + params, this.httpOptions)
  }

  async getApiProvidencias<T>(params) {

    await this.tokenService.validarToken()
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.tokenService.token.access_token,
        'Content-Type': 'application/json',
        'cartorio': this.chosenCartorio.userId,
      }),
    };
    return this.http.get<T>(environment.url + params, httpOptions).toPromise()
  }

  getApiCepAntigo<T>(params): Observable<T> {
    return this.http.get<T>(environment.url + params, this.httpOptions)
  }

  postApiAntigo<T>(params, body): Observable<T> {
    return this.http.post<T>(environment.url + params, body, this.httpOptions)
  }


  putApiAntigo(url, body) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.token,
        'Content-Type': 'application/json',
        'cartorio': this.chosenCartorio.userId
      }),
    };
    return this.http.put<string>(environment.url + url, body, httpOptions).toPromise()
  }

  setHeaderAntigo(token) {
    this.httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': token,
          'cartorio': this.chosenCartorio.userId
        }
      ),
    }
  }

  setHeaderBuscaAntigo(token, cpf) {
    this.httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': token,
          'cartorio': this.chosenCartorio.userId,
          'identificacao': cpf
        }
      ),
    }
  }

  /* Api's layout novo */


  setCartorio(cartorio: Cartorio) {
    this.chosenCartorio.userId = cartorio.codigo
    this.favIcon.href = cartorio.urlLogo
    this.title.innerHTML = cartorio.nome
    this.chosenCartorio.cep = cartorio.cep
    this.chosenCartorio.urlLogo = cartorio.urlLogo
    localStorage.setItem('logo', this.chosenCartorio.urlLogo)
  }

  /* getToken(params: string) {
    return this.http.get(`${environment.url}/api/v1/sys/${params}`, {
      headers: new HttpHeaders().set('dominio', 'solicitacoes2.cartoriotoledo.com.br')
    })
  } */

  getToken(params: string, codigo: string) {
    let c = codigo
    let url = window.location.hostname
    if (url.includes('solicitacoeshml')) {
      url = url.replace('solicitacoeshml.', '')
    } else if(url.includes('solicitacoes2')){
      url = url.replace('solicitacoes2.', '')
    }else{
      url = url.replace('solicitacoes.', '')
    }
    return this.http.get(`${environment.url}/api/v1/sys/${params}`, {
      headers: new HttpHeaders().set(`${c ? 'codigocartorio' : 'dominio'}`, c ? c : url)
    })
  }

  getCartorioAtual(params: string, codigo: string) {
    let tkn = localStorage.getItem('token')
    this.setToken(tkn, codigo ? codigo : window.location.hostname, codigo ? true : false)
    return this.http.get(`${environment.url}/api/v1/secure/${params}`, this.httpOptions)
  }

  getApi(params: string) {
    let tkn = localStorage.getItem('token')
    this.setToken(tkn)
    return this.http.get(`${environment.url}/api/v1/secure/${params}`, this.httpOptions)
  }

  baixarOrcamentoPdf(params: string) {
    let tkn = localStorage.getItem('token')
    this.setToken(tkn)
    const httpOptions = {
      responseType: 'blob' as 'json',
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + tkn,
      })
    };
    return this.http.get(`${environment.url}/api/v1/secure/${params}`, httpOptions)
  }

  getApiListaEstados(params: string) {
    return this.http.get(`${environment.url}/api/v1/${params}`, this.httpOptions)
  }

  postApiUpload(params: string, body) {
    return this.http.post(`${environment.url}/api/v1/${params}`, body, this.httpOptions)
  }

  deleteApi(params: string) {
    let tkn = localStorage.getItem('token')
    this.setToken(tkn)
    return this.http.delete(`${environment.url}/api/v1/secure/${params}`, this.httpOptions)
  }

  putApi(params: string, body) {
    let tkn = localStorage.getItem('token')
    this.setToken(tkn)
    return this.http.put(`${environment.url}/api/v1/secure/${params}`, body, this.httpOptions)
  }

  postApi(params: string, body) {
    let tkn = localStorage.getItem('token')
    this.setToken(tkn)
    return this.http.post(`${environment.url}/api/v1/secure/${params}`, body, this.httpOptions)
  }

  setToken(token, dominio?, hasParam?) {
    if (hasParam) {
      this.httpOptions = {
        headers: new HttpHeaders(
          {
            'Authorization': 'Bearer ' + token,

          }
        )
      }
    } else {
      this.httpOptions = {
        headers: new HttpHeaders(
          {
            'Authorization': 'Bearer ' + token,

          }
        )
      }
    }
  }


}





