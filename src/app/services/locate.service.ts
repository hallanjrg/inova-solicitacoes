import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

import { LoadingService } from './loading.service';

@Injectable({
  providedIn: 'root'
})
export class LocateService {

  public ufs: any[] = []
  public cidades: any[] = []

  public estados //nova
  public municipios // nova

  constructor(public apiService: ApiService,
    public loadingService: LoadingService) { }

  getAllUfs() { // velho novo é o getEstados()
    this.loadingService.isActive = true
    this.apiService.getApiLocate('localidades/estados?orderBy=nome').subscribe((res: any) => {
      this.ufs.push(res)
      this.loadingService.isActive = false
    }, err => {
      this.loadingService.isActive = false
    })
  }

  getDistrict(uf) { // velho (o  novo é o getMunicipiosPorEstado())
    this.loadingService.isActive = true
    this.cidades = []
    this.apiService.getApiLocate(`localidades/estados/${uf}/municipios?orderBy=nome`).subscribe((res: any) => {
      this.cidades.push(res)
      this.loadingService.isActive = false
    }, err => {
      this.loadingService.isActive = false
    })
  }

  getEstados() { // novo
    this.apiService.getApiListaEstados(`dominio/estados`).subscribe(res => {
      this.estados = Object.entries(res)
      let newEstados = []
      this.estados.forEach(element => {
        newEstados.push(element[0])
      });
      this.estados = newEstados.sort()
    })
  }

  getMunicipiosPorEstado(uf) { //novo
    this.apiService.getApiListaEstados(`dominio/municipios?uf=${uf}`).subscribe(res => {
      this.municipios = res
    })
  }
}
