import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpHeaders } from '@angular/common/http';
import { LoadingService } from './loading.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BuscaCepService } from './busca-cep.service';
import { ProvidenciasService } from './providencias.service';
import { MultiSolicitationService } from './multi-solicitation.service'
import { AddedFiles } from '../models/addedFiles';
import { Registro } from 'src/app/models/registro';
import { Message } from 'src/app/models/message';

interface arrayConsulta {
  codigoSimples: number;
  dataCriacao: Date;
  codigo: number;
  data_criacao: Date;
  codigo_simples: number;
  resumo: [
    {
      descricao: string;
      id: number;
      qtd: number;
    }
  ],
  solicitacoes: [
    {
      data_ato: string;
      codigo: number;
      creation_date: string;
      folha_ato: string;
      forma_entrega: number;
      livro_ato: string;
      mensagem: string;
      nome_partes: string;
      entrega: number;
      solicitante: {
        email: string;
        nome: string;
        cpf_cnpj: string;
        telefone: string;
      },
      status: string;
      tipo_ato: string;
      valor_frete: number;
      valor_solicitacao: number;
    }
  ],
  tipo_retirada: string;

}

@Injectable({
  providedIn: 'root'
})
export class InfoService {
  public registros: Array<Registro> = [];
  public files: Array<AddedFiles> = []
  public messages: Array<Message> = []
  buscarSolicitacao = false
  temEndereco = false
  access_token
  clicked = false
  opcaoEntregaSelecionada: string = 'Retirar no cartório';
  forma_entrega
  opcoesEntrega: Array<string> = ['Retirar no cartório', 'Entregar no endereço'];
  arraySolicitacoes: arrayConsulta
  codigo_solicitacao: number = null
  email: string = null;
  cpfSolicitacao: string = null
  base64 = []
  summaryCheck: arrayConsulta
  error = false

  constructor(private apiService: ApiService,
    public loadingService: LoadingService,
    public router: Router,
    public buscaCepService: BuscaCepService,
    public providenciasService: ProvidenciasService,
    public multiSolicitationService: MultiSolicitationService,
    private route: ActivatedRoute) {
    this.codigo_solicitacao = this.route.snapshot.queryParams['codigo'];
    this.email = this.route.snapshot.queryParams['email'];
    if(this.codigo_solicitacao && this.email){
      this.consultar(this.codigo_solicitacao);
    }
     }

  gerarPedido(inputs) {
    this.clicked = true
    let dataWithAddress: Object
    let dataWithoutAddress: Object
    switch (this.buscaCepService.qtdSolicitacoes) {
      case 1:
        dataWithAddress = this.multiSolicitationService.dados1WA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        dataWithoutAddress = this.multiSolicitationService.dados1WOA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        break;
      case 2:
        dataWithAddress = this.multiSolicitationService.dados2WA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        dataWithoutAddress = this.multiSolicitationService.dados2WOA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        break;
      case 3:
        dataWithAddress = this.multiSolicitationService.dados3WA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        dataWithoutAddress = this.multiSolicitationService.dados3WOA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        break;
      case 4:
        dataWithAddress = this.multiSolicitationService.dados4WA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        dataWithoutAddress = this.multiSolicitationService.dados4WOA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        break;
      case 5:
        dataWithAddress = this.multiSolicitationService.dados5WA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        dataWithoutAddress = this.multiSolicitationService.dados5WOA(inputs, this.forma_entrega, this.buscaCepService.valorServico, this.buscaCepService.frete)
        break;
    }

    this.apiService.setHeaderAntigo(this.access_token)
    if (this.forma_entrega === 2) {
      this.apiService.postApiAntigo<any>('/solicitacoes', dataWithAddress).subscribe(result => {
        this.uploadArquivo(result.solicitacao.codigo)
        this.router.navigate(['/finish/' + result.solicitacao.codigo])
        this.clicked = false
      }, error => {
        this.loadingService.isActive = false
        this.clicked = false
      })
    } else {
      this.apiService.postApiAntigo<any>('/solicitacoes', dataWithoutAddress).subscribe(result => {
        this.uploadArquivo(result.solicitacao.codigo)
        this.router.navigate(['/finish/' + result.solicitacao.codigo])
        this.clicked = false
      }, error => {
        this.loadingService.isActive = false
        this.clicked = false
      })
    }
  }

  uploadArquivo(userCode) {
    this.apiService.postFork('/solicitacoes/' + userCode + '/uploads', this.base64).subscribe(res => {
      this.loadingService.isActive = false
      this.router.navigate(['/finish/' + userCode])
    })
  }

  handleSelect(event: any) {
    let array = Array.from(event.target.files)
    array.forEach((element: any) => {
      if (element.size < 10000000) {
        this.upload(element)
      } else {
        alert(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`)
      }
    })
    event.target.value = ''
  }

  upload(fileList: any) {
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.apiService.postApiUpload('bucket/upload', formData).subscribe((res: AddedFiles) => {

      this.files.push({
        codigoArquivo: res.codigoArquivo,
        name: fileList.name
      })
    }, err => {
      //
    })
  }

  consultar(id) {
    this.apiService.getApi(`solicitacaocertidao/obter?codigoSimples=${id}&email=${this.email}`).subscribe((res: any) => {
      this.error = false
      this.arraySolicitacoes = res
      this.buscarSolicitacao = true
      this.providenciasService.getPendingItems(res.codigo)
      this.getRegistro(res.codigo)
      this.getChat(res.codigo)
      this.temEndereco = true
    }, err => {
      this.temEndereco = false
      this.buscarSolicitacao = false
      this.error = true
    })
  }

  getRegistro(codigo: any){
    this.apiService
      .getApi(`solicitacaocertidao/listarregistros?codigoSolicitacao=${codigo}`)
      .subscribe((res: Array<Registro>) => {
        res.sort((a: { dataCriacao: any }, b: { dataCriacao: any }) =>
          b.dataCriacao.localeCompare(a.dataCriacao)
        );
        this.registros = res;
      });
  }

  getChat(codigo: String){
    this.apiService
      .getApi(`chat/listar?codigoSolicitacao=${codigo}`)
      .subscribe((res: any) => {
        this.messages = res;
        this.scrollToBottom()
      });
  }

  scrollToBottom() {
    let messageBody = document.querySelector('#collapseChat')
    if (messageBody) {
      setTimeout(() => {
        messageBody!.scrollTop = messageBody!.scrollHeight - messageBody!.clientHeight
      }, 1);
    }
  }



  getUrl(url) {
    let index = url.lastIndexOf('/')
    let fileName = url.slice(index + 1)
    return fileName
  }




}
