import { Injectable, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable()
export class SolicitacaoService implements OnInit {
    private step: Subject<number>;
    private data;
    public solicitacaoGerada
    public clicked = false
    public resetForm = new BehaviorSubject<boolean>(false)
    public novaSolicitacao = {
        contagem: [''],
        codigoSimples: [''],
        dados: this.fb.group({
            tipoAto: [''],
            termo: [''],
            folha: [''],
            livro: [''],
            dataAto: [''],
            nomePartes: ['']
        }),
        cartorioCertidao: [''],
        cidadeCertidao: [''],
        estadoCertidao: [''],
        filiacaoCertidao: this.fb.group({
            filiacao1: [''],
            filiacao2: ['']
        }),
        identificacaoCertidao: this.fb.group({
            cpf: [''],
            dataNascimentoCasamento: [''],
            nomeCompleto: [''],
            nomeConjuge1: [''],
            nomeConjuge2: ['']
        }),
        endereco: this.fb.group({
            cep: [''],
            logradouro: [''],
            numero: [''],
            complemento: [''],
            uf: [''],
            cidade: [''],
            bairro: [''],
            retirar: [0]
        }),
        solicitante: this.fb.group({
            nome: [''],
            cpfCnpj: [''],
            email: [''],
            telefone: [''],
        }),
        mensagem: ['']
    }

    constructor(private fb: FormBuilder) {
      
        this.resetForm.subscribe(res => {
            if (res) {
                this.novaSolicitacao = {
                    contagem: [''],
                    codigoSimples: [''],
                    dados: this.fb.group({
                        tipoAto: [''],
                        termo: [''],
                        folha: [''],
                        livro: [''],
                        dataAto: [''],
                        nomePartes: ['']
                    }),
                    cartorioCertidao: [''],
                    cidadeCertidao: [''],
                    estadoCertidao: [''],
                    filiacaoCertidao: this.fb.group({
                        filiacao1: [''],
                        filiacao2: ['']
                    }),
                    identificacaoCertidao: this.fb.group({
                        cpf: [''],
                        dataNascimentoCasamento: [''],
                        nomeCompleto: [''],
                        nomeConjuge1: [''],
                        nomeConjuge2: ['']
                    }),
                    endereco: this.fb.group({
                        cep: [''],
                        logradouro: [''],
                        numero: [''],
                        complemento: [''],
                        uf: [''],
                        cidade: [''],
                        bairro: [''],
                        retirar: [0]
                    }),
                    solicitante: this.fb.group({
                        nome: [''],
                        cpfCnpj: [''],
                        email: [''],
                        telefone: [''],
                    }),
                    mensagem: ['']
                }
            }
        })
    }

    ngOnInit() {

    }

    change(step: number) {
        this.step.next(step);
    }

    onStepChange() {
        return this.step;
    }

}
