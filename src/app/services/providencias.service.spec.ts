import { TestBed } from '@angular/core/testing';

import { ProvidenciasService } from './providencias.service';

describe('ProvidenciasService', () => {
  let service: ProvidenciasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProvidenciasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
