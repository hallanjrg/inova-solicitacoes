import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from './api.service';
import { InfoService } from './info.service';
import { LoadingService } from './loading.service';
import { ToastService } from './toast/toast-service';
import { TokenService } from './token.service';

interface tokenInterface {
  access_token: string;
  expires_in: number;
  token_type: string;
}
export interface PendenciaInterface {
  forEach(arg0: (element: any) => void);
  codigo: string;
  data_pendencia: Date;
  texto_pendencia: string;
  resolvido: number;
}

@Injectable({
  providedIn: 'root'
})
export class ProvidenciasService {

  public resProvidencia: PendenciaInterface = null
  public temProvidencia = null
  public blockTableView = false
  public atualizarTela = new BehaviorSubject<boolean>(false)
  public providenciaSelecionada = null
  public allPend: Array<any> = []
  public hideBadge: boolean = false


  constructor(private apiService: ApiService,
    private loadingService: LoadingService,
    private tokenService: TokenService,
    private toast: ToastService) { }

  providencia(msg, sol) {
    const data = {
      codigoSolicitacao: sol.codigo,
      codigo: sol.providencia.codigo,
      dataPendencia: sol.providencia.dataPendencia,
      notificacoes: null,
      sucesso: null,
      resolvido: null,
      textoPendencia: msg,
      msg: null,
      tipoArquivo: '',
    }
    if (msg == '') {
      this.toast.show('Mensagem obrigatória')
    } else {
      this.loadingService.isActive = true
      this.apiService.postApi(`pendencia/save`, data).subscribe(res => {
        this.loadingService.isActive = false
      }, err => {
        this.loadingService.isActive = false
        setTimeout(() => {
        }, 2000);

      })
    }
  }

  getPendingItems(codigo: string) {
    this.apiService.getApi(`pendencia/listar?codigoSolicitacao=${codigo}`).subscribe((res: any) => {
      this.loadingService.isActive = false
      this.resProvidencia = res
      if (!res[0]?.codigo) {
        this.blockTableView = true
      } else {
        this.blockTableView = false
      }
      res.forEach((element) => {
        if (element.resolvido === 0) {
          this.allPend.push(element.codigo)
        }
      });
      if (this.allPend.length !== 0) {
        this.hideBadge = false
      }
      this.loadingService.isActive = false
    }, err => {
      this.loadingService.isActive = false
    })
  }

  async getProvidencias(info) {
    this.hideBadge = true
    this.allPend = []
    this.blockTableView = false
    await this.tokenService.getToken().subscribe((result: tokenInterface) => {
      this.apiService.token = result.access_token
      this.apiService.setHeaderAntigo(result.access_token)
    })
    this.apiService.getApiProvidencias('/solicitacoes/' + info.codigo + '/pendencias').then((res: PendenciaInterface) => {
      this.resProvidencia = res
      if (!res[0]?.codigo) {
        this.blockTableView = true
      } else {
        this.blockTableView = false
      }
      res.forEach((element) => {
        if (element.resolvido === 0) {
          this.allPend.push(element.codigo)
        }
      });
      if (this.allPend.length !== 0) {
        this.hideBadge = false
      }
      this.loadingService.isActive = false
    })
  }


}
