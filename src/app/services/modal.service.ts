import { Injectable } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Injectable()
export class ModalService {
    public modalRef: BsModalRef;
    public actionOnHide: boolean = false

    constructor(
        private readonly modal: BsModalService
    ) { }

    open(component: any, args?: any) {
        this.modalRef = this.modal.show(component, args);
    }

    close() {
        if (this.modalRef) {
            this.modalRef.hide();
            this.modalRef = null;
        }
    }

    changeModal(component: any, args?: any) {
        this.close();
        this.open(component, args);
    }
}