import { TestBed } from '@angular/core/testing';

import { MultiSolicitationService } from './multi-solicitation.service';

describe('MultiSolicitationService', () => {
  let service: MultiSolicitationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MultiSolicitationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
