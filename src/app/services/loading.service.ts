import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  
  public isActive = false;
  public loadButtonActive = false

  constructor() { }
}
