import { Component, ChangeDetectorRef } from '@angular/core';
import { from } from 'rxjs';
import { ApiService } from './services/api.service';
import { LoadingService } from './services/loading.service';
import { Token } from 'src/app/models/token-interface'
import { Cartorio } from 'src/app/models/cartorio'
import { ActivatedRoute, Router } from '@angular/router';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'inova-pagamentos';

  get loading() {
    return this.loadingService.isActive
  }
  constructor(
    public loadingService: LoadingService,
    private cdr: ChangeDetectorRef,
    public apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }
  ngOnInit(): void {
  /*   this.apiService.getToken(`token`, this.route.snapshot.queryParams['codigoCartorio']).subscribe((res: Token) => {
      localStorage.setItem('token', res.token);
      this.apiService.getCartorioAtual(`cartorio/get`).subscribe((res: Cartorio) => {
        this.apiService.setCartorio(res)
      })
    })
    this.apiService.getCartorio(window.location.href) */
  }
}
