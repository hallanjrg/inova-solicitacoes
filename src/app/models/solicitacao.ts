import { AddedFiles } from 'src/app/models/addedFiles'

export class NewSolicitacao {
    certidaoDigital?: boolean = false
    canal?: string = '2';
    arquivos?: any;
    cartorioCertidao?: string = '';
    nomeCartorio?: string = '';
    cidadeCertidao?: string = '';
    estadoCertidao?: string = '';
    dados?: Array<DadosAto> = [];
    dadosSolicitante?: Solicitante = new Solicitante;
    endereco?: Recebimento = new Recebimento;
    enderecoSolicitante?: Recebimento = new Recebimento;
    filiacaoCertidao?: Filiacao = new Filiacao;
    origemSolicitacao?: string = '';
    identificacaoCertidao?: IdentificacaoCertidao = new IdentificacaoCertidao;
    tipoSolicitacao?: string = '';
    mensagem?: string = '';
    tipoRetirada?: number = 1;
    valorFrete?: number = 0;
    qtdAverbacao?: number = 0;
    valorSolicitacao?: number = 0;
}

export class DadosAto {
    nomePartes: string = '';
    tipoAto: string = '';
    dataAto: string = '';
    livro: string = '';
    folha: string = '';
    termo?: string = '';
    anexos: File[];
    contagem?: number = 0;
}

export class Solicitante {
    nome: any = '';
    cpfCnpj: string = '';
    email: string = '';
    telefone: string = '';z
}

export class Recebimento {
    retirar?: number = 0;
    bairro: string = '';
    cep: string = '';
    logradouro: string = '';
    numero: string = '';
    complemento: string = '';
    cidade: string = '';
    uf?: string = '';
}

export class Filiacao {
    filiacao1: string = '';
    filiacao2: string = '';
}

export class IdentificacaoCertidao {
    cpf: string = '';
    dataNascimentoCasamento: Date = new Date();
    nomeCompleto: string = '';
    nomeConjuge1: string = '';
    nomeConjuge2: string = '';
}
