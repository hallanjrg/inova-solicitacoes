export class Registro {
  codigo: number = 0;
  dataCriacao: Date = null;
  descricao: string = '';
  descricaoTipoAssuntoOutros: string = '';
  documentos: Array<String> = [];
  message: string = '';
  statusEnvio: string = '';
  tipoAssunto: string = ''
}
