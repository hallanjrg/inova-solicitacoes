export interface Message {
  dataCriacao: Date;
  mensagem: string;
  linkArquivo: string;
  codigoSolicitacao: string;
  remetente: string;
  msgSolicitante: boolean;

}
