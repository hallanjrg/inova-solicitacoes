import { FormGroup } from "@angular/forms";

export class Orcamento {
    valor: number;
    tipoEscritura: string;

    static novo(form: FormGroup) {
        const o = new Orcamento();
        o.valor = form?.get('valor').value;
        o.tipoEscritura = 'ITBI';
        return o;
    }
}

export class ResultOrcamento {
    codigo: string;
    codigoSimples: string;
    valorInformado: number;
    valorRegistro: number;
    tipoEscritura: string;
    valorTipoEscritura: number;
    valorEscritura: number;
    valorTotal: number;
    linkPdf: string;
}
