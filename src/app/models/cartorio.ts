export interface Cartorio {
    codigo: string,
      nome: string,
      nomeSocial: string,
      dominio: string,
      pagamentoUrl: string,
      urlLogo: string,
      cep: string
      cidade:string,
      estado:string
  }
