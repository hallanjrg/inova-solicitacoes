export interface tokenInterface {
  access_token: string;
  expires_in: number;
  token_type: string;
}

export interface Token {
  token: string;
}