import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public itens: Array<any> = [];

  constructor() {
    let c = JSON.parse(localStorage.getItem('cartorio'));
    this.itens = [
      {
        descricao: 'Nova solicitação',
        rota: `/solicitacao-simples/${c.codigoCartorio}`,
        icone: 'bi bi-clipboard-plus'
      },
      {
        descricao: 'Consultar solicitações',
        rota: `/solicitacao-simples/consulta/${c.codigoCartorio}`,
        icone: 'bi bi-file-earmark-person'
      }
    ]
  }

  ngOnInit(): void {
  }

}
