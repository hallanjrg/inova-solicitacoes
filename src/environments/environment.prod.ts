export const environment = {
  production: true,
  url: 'https://app2.inovacartorios.com.br/inova-cartorios',
  locateUrl: 'https://servicodados.ibge.gov.br/api/v1/',
  urlFirmaToken: 'https://apievolution.escriba.com.br/oauth/token?username=publico&grant_type=password&tenant=41&password=publico&username=publico',
  urlFirma: 'https://apievolution.escriba.com.br/e-websystems-router/v1/consultar?uri=/v1/assina/cartao-assinatura/'
};
