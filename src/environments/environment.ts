// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  /*   url: 'https://f2rkl509o9.execute-api.us-west-2.amazonaws.com/Dev' */

  //  url: 'https://ut8oc1ljcf.execute-api.us-west-2.amazonaws.com/Prod'
  url: 'https://hml.inovacartorios.com.br/inova-cartorios',
  locateUrl: 'https://servicodados.ibge.gov.br/api/v1/',
  urlFirmaToken: 'https://apievolution.escriba.com.br/oauth/token?username=publico&grant_type=password&tenant=41&password=publico&username=publico',
  urlFirma: 'https://apievolution.escriba.com.br/e-websystems-router/v1/consultar?uri=/v1/assina/cartao-assinatura/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
